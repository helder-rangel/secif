# REGULAMENTO ESPECÍFICO DA MOSTRA DE TALENTOS   


# História do gestor  
Como gestor, determino que todos os estudantes, docentes,  
técnicos administrativos e prestadores de serviço do Campus  
poderão participar expressando-se através das artes musicais,  
cênicas, visuais ou plásticas.  
Determino que cada competidor poderá se inscrever em 2 atividades,  
com limites de vagas. A mostra de talentos não acumulará pontos para  
a competição Secif.


# História do julgador 
Irei dar notas para cada competidor. A pontuação não será atribuída à competição Secif.  

# História do competidor  

Como participante da mostra quando for fazer minha inscrição poderei ver, eventuais inscritos.  
Caso não haja vaga, poderei entrar no cadastro de reserva e ser chamado de acordo com  
ordem do cadastro, com ordem de prioridade para os que se inscreveram primeiro.  
Quando as inscrições forem finalizadas serei avisado na tela do aplicativo e por e-mail.  
Serei informado que todo o material necessário para a apresentação ou instalação  
será de minha responsabilidade. O IFPB – Campus João Pessoa dispõe de Equipamento  
de Sonorização que poderá ser utilizado nas mostras, cujas especificações e  
disponibilidade podem ser avaliadas junto à Coordenação de Multimeios.  
