# REGULAMENTO ESPECÍFICO DO LANÇAMENTO DE FOGUETES  

# História do gestor  
Quero que participem da modalidade de lançamento de foguetes  
os estudantes dos Cursos Técnicos Integrados. Sendo que cada  
curso poderá inscrever até 5 alunos. No ato da inscrição, o competidor  
deve ser comprometer a participar do treinamento e testes preliminares.  

Cada grupo poderá executar 3 lançamentos.

# História do julgador  
Irei medir o alcance de cada lançamento e informar ao sistema que irá  
classificar os competidores.  

# História do competidor  
Como competidor, quando for fazer minha inscrição poderei ver eventuais inscritos.  
Caso não haja vaga, poderei entrar no cadastro de reserva e ser chamado de acordo com  
ordem do cadastro, com prioridade para os que se inscreveram primeiro.  
