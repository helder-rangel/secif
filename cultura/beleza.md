# REGULAMENTO ESPECÍFICO DA GAROTA E GAROTO SECIF 2017  

# História do gestor  
Como gestor determino que a escolha do garoto e garota Secif deve ser entre estudantes dos  
Cursos Técnicos e Integrados do ensino médio.  Quem for menor de 18 anos, deverá enviar  
documento de identidade do responsável e assinatura de documento que autorize sua participação.  

Coloco a limitação de 4 garotos e 4 garotas inscritos para cada série dos cursos. No caso de  
não preenchimento das vagas por uma série,  a(s) vaga(s) poderá(ão) ser preenchida(s) por outra(s) série(s) do próprio curso.  
ENTRADA NA PASSARELA. Determino que na primeira entrada os concorrentes usem traje casual de  
livre escolha. Já na segunda, deve-se usar o traje esportivo, de competição, também de livre  
escolha para garotas e garotos. Na terceira entrada, o traje deve representar o perfil do  
respectivo curso.  

# História do julgador  
Como julgador(a), darei notas de 0 a 100 nos critérios de elegância e porte na passarela,  
desenvoltura na passarela, criatividade nos trajes, simpatia e interação com o público,  
qualidade da produção, beleza e desembaraço na comunicação da etapa final.  

Quando o sistema apurar o resultado das entradas preliminares, os 5 candidatas e os 5  
candidatos melhor colocados(as) participarão da etapa final.  

Então, na etapa final, farei uma pergunta de tema geral, quando darei uma nota de 0 a 100 à  
resposta de cada concorrente. A essa nota, somarei a nota da etapa anterior, definindo assim  
classificação dos alunos e alunas individualmente e por curso.  

Caso haja empate, na etapa preliminar, vou considerar a maior pontuação obtida na ordem  
decrescente dos critérios simpatia e interação com o público, qualidade da produção, elegância  
e porte na passarela, desenvoltura na passarela, criatividade nos trajtes e beleza.  
Caso o empate permaneça, a comissão julgadora decidirá por voto aberto. 

Darei medalha aos três primeiros colocados, que receberão faixa de Garota e Garoto Secif.  
Também darei medalha aos 3 cursos melhores representados. Vou creditar os pontos  
de acordo com a colocação dos concorrentes e atribuí-las a cada curso participante.  

# História do líder  
Como líder de turma irei selecionar os alunos que irão participar do concurso de beleza,  
entre os colegas de curso.  
Ao fazer cada seleção, o colega será notificado por e-mail.  


# História do competidor  

Como competidor, farei minha inscrição usando usuário e senha do Suap. Ao fazer inscrição, caso eu seja menor de 18 anos,  
deverei encaminhar pelo aplicativo a cópia de identidade de meu responsável, além da assinatura  
dele em documento que autorize a minha participação.  
No ato da inscrição, verei a lista dos concorrentes inscritos até então.  
Ao final do prazo de inscrição, serei avisado na tela do aplicativo quais são  
os concorrentes, de ambos os sexos,  como também serei informado disso por e-mail.  

Ao finalizar minha inscrição, serei avisado(a) na tela do aplicativo e por e-mail sobre local e horário do ensaio.

# Regras da competição

Poderão participar da escolha da Garota e do Garoto SECIF estudantes dos Cursos Técnicos
Integrados ao Ensino Médio.  

Os participantes menores de 18 anos deverão no ato da inscrição, preencher formulário de inscrição,  
imprimí-lo e solicitar a assinatura do representante legal idêntica a do documento de identidade.  

Após o formulário assinado, o participante deverá entregá-lo à Comissão organizadora  
da SECIF junto à cópia da identidade do seu representante legal  
(www.joaopessoa.ifpb.edu.br/secif).  

As inscrições serão realizadas por meio do Sistema de Inscrições da SECIF 2017. (www.joaopessoa.ifpb.edu.br/secif)  
Cada Curso poderá inscrever 04 (quatro) garotas e 04 (quatro) garotos, prioritariamente  
representando cada série do respectivo Curso.  

Caso não haja representação de uma determinada série, esta vaga poderá ser  
ocupada por candidato (a)s de outras series do próprio curso.  

Na fase preliminar as entradas na passarela terão as seguintes configurações;  
Na primeira entrada, será o traje casual de livre escolha;  
Na segunda o traje esportivo (Competição) de livre escolha para as garotas e para os garotos;  
Na terceira, um traje que represente o perfil do seu curso.

O julgamento dos candidatos (as) será realizado por uma comissão formada por 05 (cinco) jurados,  
sendo 03(três) externos e 02 (dois) internos convidados pela comissão Organizadora Geral.  

Para cada entrada, os jurados deverão atribuir notas de 0 a 100 para os seguintes critérios:  
Elegância e porte na Passarela;  
Desenvoltura na passarela;  
Criatividade nos Trajes;  
Simpatia e interação com o público;  
Qualidade da Produção;  
Beleza;
Desembaraço na comunicação, na etapa final.  


Apurado o resultado das entradas preliminares, os 05 (cinco) candidatos e as 05 (cinco) candidatas  
melhores colocado (a)s participarão da etapa final.  

Na etapa final haverá uma pergunta de tema geral em que será avaliado. A nota (de 0 a 100) se somará  
à pontuação acumulada na etapa preliminar em que cada candidato(a) obteve.  

Atribuídas as notas da etapa final, serão divulgadas a classificação individual e a classificação por  
curso no Garota e Garoto SECIF.  

Caso na etapa preliminar haja empate de pontuação para classificar os 05 que irão para  
a etapa final,  será considerado(a) a maior pontuação obtida na ordem decrescente  
dos seguintes critérios: Simpatia e  Interação com o público, qualidade da produção, elegância  
e porte na passarela, desenvoltura na passarela,  criatividade nos trajes e Beleza.  

Permanecendo o empate a Comissão julgadora decidirá em voto aberto quem avançará.  
Na hipótese de ocorrer empate após a apuração de pontos pós etapa final, será vencedor aquele(a)  
que obteve melhor nota no desembaraço na comunicação. Permanecendo empate a comissão julgadora  
decidirá em voto aberto.  

Serão conferidas premiações aos três primeiros colocados do Garoto e Garota SECIF 2017, que  
receberão Faixas da Garota e Garoto SECIF 2017 e medalhas individuais. Os 03 cursos melhores  
representados também receberão as medalhas.  

Os pontos serão atribuídos aos participantes de acordo com sua colocação, sendo os  
mesmos contabilizados na Pontuação Geral atribuída a cada Curso participante da SECIF 2019;  

Após o encerramento das inscrições, ocorrerá uma reunião e um ensaio geral com todos(as)  
participantes em data, local e horário comunicados pela organização do evento.  