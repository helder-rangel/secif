# GINCANA DO CONHECIMENTO  

# História do gestor  
Como gestor, determino que todos os estudantes dos cursos técnicos integrados  
do Campus João Pessoa podem participar.  
Cada equipe concorrente ter até 8 membros, sendo prioritariamente 2 representantes  
de cada série de determinado curso, independente de turno. 

# História do julgador
Como juiz das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas.
Ao final de cada competição, informarei os tempos e registrarei no sistema.  

# História do competidor  
Como competidor, quando for fazer minha inscrição poderei ver, eventuais inscritos.  
Caso não haja vaga, poderei entrar no cadastro de reserva e ser chamado de acordo com  
ordem do cadastro, com prioridade para os que se inscreveram primeiro.  