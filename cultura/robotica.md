# REGULAMENTO ESPECÍFICO DA COMPETIÇÃO DE ROBÓTICA  

# História do gestor  
Como gestor determino que poderão participar da competição estudantes  
dos Cursos Técnicos Integrados do Campus João Pessoa. Serão permitidos  
até 5 competidores por curso.  
Quando terminar o período de inscrições, o sistema deverá informar  
na tela da aplicação e por e-mail o horário e local para a realização  
para uma reunião, quando será apresentado o monitor de cada equipe  
e será ministrado um curso.  

# História do julgador  
Como juiz das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas.
Ao final de cada competição, informarei as pontuações no sistema.  

Como julgador, poderei desclassificar membros que não cheguem,  
ao menos 30 minutos antes do início da competição.  


# História do competidor  
Como competidor, quando for fazer minha inscrição poderei ver, eventuais inscritos.  
Caso não haja vaga, poderei entrar no cadastro de reserva e ser chamado de acordo com  
ordem do cadastro, com ordem de prioridade para os que se inscreveram primeiro.  
Quando as inscrições forem finalizadas serei avisado na tela do aplicativo e por e-mail  
o horário e local de mini-curso, onde também conhecerei o monitor de minha equipe.  
