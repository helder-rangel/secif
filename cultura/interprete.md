# REGULAMENTO ESPECÍFICO DO FESTIVAL DE INTÉRPRETES  

# História do gestor
Como gestor determino que poderão participar da competição estudantes  
dos Cursos Técnicos Integrados do Campus João Pessoa.  

Serão 2 vagas para cada curso, cada inscrito deverá informar 2 canções, sem  
a possibilidade de permuta.  

# História do julgador
Irei atribuir notas de 50 a 100 para aspectos como afinação, ritmo,  
musicalidade, interpretação, postura e criatividade.  


# História do competidor  
Como competidor, quando for fazer minha inscrição poderei ver, eventuais inscritos.  
Caso não haja vaga, poderei entrar no cadastro de reserva e ser chamado de acordo com  
ordem do cadastro, com prioridade para os que se inscreveram primeiro.  
Deverei informar os nomes de  2 canções que poderei interpretar.
