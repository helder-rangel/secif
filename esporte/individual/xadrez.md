# REGULAMENTO ESPECÍFICO DO XADREZ    

---------------------------------------------------------------------------------------------
## História do gestor 

A competição será realizada em dois torneios: um por equipes – com 05 (cinco) tabuleiros para
cada equipe – e outro individual.  
Cada equipe, nos naipes masculino e feminino, será composta por, no mínimo 02 (dois)  
e no máximo, 05 (cinco) alunos.  
As equipes que inscreverem menos que 05 (cinco) atletas, respeitando-se o mínimo  
estabelecido no parágrafo anterior, perderão a pontuação referente às partidas em que  
não houver atleta(s) por WxO.  

O torneio por equipes será disputado pelo sistema Round Robin (todos contra todos), exceto  
se o número de equipes inscritas for superior a 07 (sete), ocasião em que a competição será  
pelo sistema suíço, em 06 (seis) rodadas.  
O torneio individual será disputado pelo sistema suíço, em 06 (seis) rodadas, com a utilização  
do programa de emparceiramento Swiss-Manager, recomendado pela FIDE, em ambas as  
competições.  
Do Torneio Individual somente participarão os atletas que compuserem as equipes.  
Será aplicada a restrição de emparceiramento a jogadores do mesmo curso.  
A contagem dos pontos será feita:  

*Por alguma razão posso remover um participante da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador  


Como juiz das competições serei notificado por email quais são as competições que estarei presente
 e qual será minha função em cada uma delas.
Ao final de cada competição, informarei os tempos e registrarei no sistema.

*Em caso de falta de um dos competidores, poderei inserir no time alguém que esteja
na lista de espera?*
*Caso tal participante quebre alguma regra no torneio individual, ele será expulso do torneio?*

---------------------------------------------------------------------------------------------
## História do competidor  

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei
umas das modalidades de xadrez individual.

*Poderei cancelar minha inscrição?*

---------------------------------------------------------------------------------------------