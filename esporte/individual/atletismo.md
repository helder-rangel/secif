# REGULAMENTO ESPECÍFICO DO ATLETISMO   

---------------------------------------------------------------------------------------------
## História do gestor 
Cada curso poderá inscrever no máximo 20 (vinte) alunos/atletas em cada gênero, sendo 2  
(dois) alunos-atletas por prova e 1 (uma) equipe em cada prova de revezamento.  
Cada aluno-atleta poderá participar de no máximo 3 (três) provas individuais e dos  
revezamentos.  
O aluno-atleta deverá comparecer ao local de competição com antecedência e devidamente  
uniformizado. Para ter condição de participação, antes do início de cada prova, deverá  
apresentar sua credencial de aluno à equipe de arbitragem.  


Receberão medalhas os atletas que obtiverem a classificação do 1o ao 3o lugar. As prova  
da serem realizadas são as seguintes para ambos os sexos    
100 metros rasos;  
200 metros rasos;  
400 metros rasos;  
800 metros rasos;  
3000 metros rasos;  
Revezamento 4 x 100 metros;  
Revezamento 4 x 400 metros;  
Salto em distância;  
Lançamento de dardo;  
O dardo feminino será o de 600g e o masculino o de 800g.  
Arremesso de peso.  
O peso feminino será o de 4kg e o masculino o de 6kg.  

*Por alguma razão posso remover um participante da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador

Como julgador das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas.
Ao final de cada competição, informarei os tempos e registrarei no sistema.

*Em caso de falta de um dos competidores, poderei inserir no time alguém que esteja
na lista de espera?*
*Caso tal participante quebre alguma regra no torneio individual, ele será expulso do torneio?*

---------------------------------------------------------------------------------------------
## História do competidor  

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei
umas das modalidades de atletismo individuais.

*Poderei cancelar minha inscrição, ou trocar de equipe?*
*Se existir lista de espera, poderei me inscrever mesmo inscrito em uma competição invidiual?*

---------------------------------------------------------------------------------------------

