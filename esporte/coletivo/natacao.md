# REGULAMENTO ESPECÍFICO DA NATAÇÃO    

---------------------------------------------------------------------------------------------
## História do gestor 

Cada curso poderá inscrever no máximo 18 (dezoito) alunos/atletas em cada gênero, sendo 2  
(dois) alunos-atletas por prova e 1 (uma) equipe em cada prova de revezamento.  
Cada aluno-atleta poderá participar de no máximo 3 (três) provas individuais e dos  
revezamentos.  
O aluno-atleta deverá comparecer ao local de competição com antecedência e devidamente  
uniformizado, sendo obrigatório o uso de touca para todos os participantes, sunga para os  
homens e maiô para as mulheres. Para ter condição de participação, antes do início de cada  
prova, deverá apresentar sua credencial de aluno à equipe de arbitragem.  

As provas a serem realizadas são as seguintes para ambos os sexos:   

| Distância | Estilo |
| ----------- | ----------- |
| 25 metros |  Crawl, peito, borboleta e costas. |  
| 50 metros |  Crawl, peito, borboleta e costas. |  
| 100 metros | Crawl e Medley.|  
| 4 x 25 metros | Crawl e Medley.|  

*Será possível definir previamente os times e inseri-los no sistema?*
*Por alguma razão posso remover um participante/time, da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador  


Como julgador das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas.
Ao final de cada competição, informarei os tempos no sistema.

*Em caso de falta de um dos competidores, poderei inserir no time alguém que esteja
na lista de espera?*
*Como deverei marcar os competidores faltosos em cada partida para que não recebam o certificado?*
*Caso algum competidor não venha com a vestimenta recomendada pela CBDA, deverá ser eliminado?*


---------------------------------------------------------------------------------------------

## História do competidor  

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei umas das modalidades de natação.

*Poderei cancelar minha inscrição, ou trocar de equipe?*

