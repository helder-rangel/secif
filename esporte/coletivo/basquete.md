# REGULAMENTO ESPECÍFICO DO BASQUETEBOL  

# História do gestor  

Como gestor quero que na realização de partidas de basquetebol,  haja times femininos e masculinos. 
Cada curso poderá inscrever no mínimo 05 (cinco) e no máximo 10 (dez) alunos/atletas em cada gênero.  

Irei fazer o cadastro dos juízes da modalidade.  

> *Gestor gostaria da opção de remover aluno já inscrito?*
> *Como funciona o confronto direto para o desempate?*  

-----------------------------------------------------------------------------------------  
# História do julgador

Como juiz, serei notificado por email quais são as competições que estarei presente e qual será minha função.
Ao final da partida de basquete, informarei o placar da partida. Em caso de WO, informarei qual equipe esteve presente e a que faltou.

> *Quem vai fazer o cadastro dos juízes? Quando?* 
> *Pode ser entregue uma planilha com os dados dos juízes a serem cadastrados?*
---------------------------------------------------------------------------------------------

# História do competidor  

Como competidor(a), farei o meu cadastro, entrando com usuário do Suap. Então seleciono a competição de basquete. A tela irá mostrar os inscritos atuais. Em seguida, serei avisado pela tela do aplicativo, bem como por email, que a inscrição foi confirmada. 
Caso tenha chegado ao número máximo de inscritos, serei informado disso e poderei ficar na lista de espera ou desistir daquela modalidade na própria tela de inscrição. Ao fazer a inscrição, também serei informado sobre os nomes dos outros inscritos em minha equipe.  

> *É para criar lista de espera?*  
> *Como a lista de espera vai funcionar?*
> *É para seguir a prioridade da ordem das inscrições?*

-------------------------------------------------------------------------------------------