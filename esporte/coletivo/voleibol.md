# REGULAMENTO ESPECÍFICO DO VOLEIBALL    

---------------------------------------------------------------------------------------------
## História do gestor 

Cada curso poderá inscrever no mínimo 06 (seis) e no máximo 12 (doze) alunos/atletas em cada gênero.  


*Será possível definir previamente os times e inseri-los no sistema?*
*Por alguma razão posso remover um time da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador  


Como juiz das competições serei notificado por email quais são as competições que estarei presente
 e qual será minha função em cada uma delas.
Ao final de cada competição, informarei os tempos e registrarei no sistema.

*Em caso de falta de um dos competidores, poderei inserir no time alguém que esteja
na lista de espera?*

---------------------------------------------------------------------------------------------
## História do competidor  

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei
umas das modalidades de voleiball.

*Poderei cancelar minha inscrição, ou trocar de equipe?*
*Qual serão os critérios de escolha para os jogadores reservas?*

