# REGULAMENTO ESPECÍFICO DO ATLETISMO 

---------------------------------------------------------------------------------------------
## História do gestor 
Como gestor quero que na realização de partidas de atletismo,  haja disputas femininas e masculinas.
Cada curso poderá inscrever no máximo 20 (vinte) alunos/atletas, bem como  1(uma) equipe em cada revezamento, para ambos os gêneros.
Aluno deverá fazer credenciamento, com antecedência.


As provas a serem realizadas são as seguintes, para ambos os sexos    
- 100 metros rasos;  
- 200 metros rasos;  
- 400 metros rasos;  
- 800 metros rasos;  
- 3000 metros rasos;  
- Revezamento 4 x 100 metros;  
- Revezamento 4 x 400 metros;  
- Salto em distância;  
- Lançamento de dardo;  
- O dardo feminino será o de 600g e o masculino o de 800g.  
- Arremesso de peso.  
- O peso feminino será o de 4kg e o masculino o de 6kg.  


*Será possível definir previamente os times e inseri-los no sistema?*
*Por alguma razão posso remover um time da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador

Como juiz das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas.
Ao final de cada competição, informarei os tempos e registrarei no sistema.

---------------------------------------------------------------------------------------------
## História do competidor  

Como competidor(a), cadastrarei-me no sistema com a minha matrícula, em seguida escolherei umas das modalidades de atletismo coletivas.

*Poderei cancelar minha inscrição, ou trocar de equipe?*
*Se existir lista de espera, poderei me inscrever mesmo inscrito em uma competição individual?*

---------------------------------------------------------------------------------------------
