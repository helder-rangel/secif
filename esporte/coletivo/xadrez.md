# REGULAMENTO ESPECÍFICO DO XADREZ   

---------------------------------------------------------------------------------------------
## História do gestor 

A competição será realizada em dois torneios: um por equipes – com 05 (cinco) tabuleiros para
cada equipe – e outro individual.  
Cada equipe, nos naipes masculino e feminino, será composta por, no mínimo 02 (dois)  
e no máximo, 05 (cinco) alunos.  



*Será possível definir previamente os times e inseri-los no sistema?*
*Por alguma razão posso remover um time da competição?* 

## História do juiz  


Como juiz das competições serei notificado por email quais são as competições que estarei presente
 e qual será minha função em cada uma delas.
Ao final de cada competição, informarei os tempos e registrarei no sistema.

*Em caso de falta de um dos competidores, poderei inserir no time alguém que esteja
na lista de espera?*

---------------------------------------------------------------------------------------------  
## História do competidor  

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei
umas das modalidades de xadrez.

*Poderei cancelar minha inscrição, ou trocar de equipe?*

---------------------------------------------------------------------------------------------

