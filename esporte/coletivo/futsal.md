#REGULAMENTO ESPECÍFICO DO FUTSAL 

---------------------------------------------------------------------------------------------
## História do gestor 

Como gestor quero que na realização de partidas de futebol,  haja times do gênero masculino. Cada curso poderá inscrever no mínimo 5 (cinco) e no máximo 10 (dez) alunos/atletas.



*Será possível definir previamente os times e inseri-los no sistema?*
*Por alguma razão posso remover um time da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador  


Como juiz das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas. Ao final de cada competição, informarei os tempos e registrarei no sistema.

*Em caso de falta de um dos competidores, poderei inserir no time alguém que esteja
na lista de espera?*
*Como deverei marcar os competidores faltosos em cada partida para que não recebam o certificado?*

---------------------------------------------------------------------------------------------
## História do competidor  

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei a atividade de futsal.

*Poderei cancelar minha inscrição?*
*Qual serão os critérios de escolha para os jogadores reservas?*

