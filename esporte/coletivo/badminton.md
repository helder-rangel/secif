# REGULAMENTO ESPECÍFICO DO BADMINTON >> MAX

---------------------------------------------------------------------------------------------
## História do gestor 

Com a função de gestor do evento desejo que na realização de quaisquer eventos na modalidade de badminton, onde o sistema deverá permitir a inscrição de times mistos aplicando o limite definido no edital automaticamente.

Cada curso poderá inscrever até quatro alunas-atletas no feminino e até quatro alunos-atletas no masculino.  
Os cursos poderão ser representados nos torneios da forma a seguir:  
- Simples masculina (SM) – 2 vagas;  
- Simples feminina (SF) – 2 vagas;  
- Dupla masculina (DM) – 1 dupla;  
- Dupla feminina (DF) – 1 dupla;  
- Dupla mista (DX) – 1 dupla;  

*Será possível definir previamente os times e inseri-los no sistema?*
*Por alguma razão posso remover um time da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador

Como juiz das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas.

Ao final de cada competição, informarei os pontos de cada partida e registrarei no sistema.

*Caso algum competidor não siga as indicações da CBBd, como posso eliminá-lo(a) da competição?*
*Como deverei marcar os competidores faltosos em cada partida para que não recebam o certificado?*

---------------------------------------------------------------------------------------------  

## História do competidor    

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei umas das modalidades de badminton coletivas.

*As duplas mistas, serão escolhidas aleatoriamente?*
*Caso eu cancele minha inscrição, a vaga será preenchida de outra maneira?*


---------------------------------------------------------------------------------------------
