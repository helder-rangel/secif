# REGULAMENTO ESPECÍFICO DO HANDEBALL    

---------------------------------------------------------------------------------------------
## História do gestor 

Cada curso poderá inscrever no mínimo 07 (sete) e no máximo 14 (quatorze) alunos/atletas em cada gênero. A equipe deverá comparecer ao local da competição com antecedência e devidamente  uniformizada. Para ter condição de participação, antes do início de cada jogo, deverá  apresentar suas credenciais de aluno à equipe de arbitragem.  


*Será possível definir previamente os times e inseri-los no sistema?*
*Por alguma razão posso remover um time da competição?* 

---------------------------------------------------------------------------------------------  
## História do julgador  


Como juiz das competições serei notificado por email quais são as competições que estarei presente  e qual será minha função em cada uma delas. Ao final de cada competição, informarei o placar no sistema.

*Em caso de falta de um dos competidores, poderei inserir no time alguém que esteja na lista de espera?*
*Como deverei marcar os competidores faltosos em cada partida para que não recebam o certificado?*

---------------------------------------------------------------------------------------------

## História do competidor  

Como competidor(a), cadastrarei ao sistema com a minha matrícula, em seguida escolherei umas das modalidades de handball.

*Poderei cancelar minha inscrição?*
*Qual serão os critérios de escolha para os jogadores reservas?*

