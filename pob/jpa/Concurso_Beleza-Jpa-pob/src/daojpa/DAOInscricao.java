package daojpa;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Candidato;
import modelo.Inscricao;

public class DAOInscricao extends DAO<Inscricao> {

	public DAOInscricao () {
		
	}


	public Inscricao read(Object chave) {
		try{
			Candidato candidato = (Candidato) chave;
			Query q = manager.createQuery("select i from Inscricao i where i.candidato = :c");
			 q.setParameter("c", chave);
			return (Inscricao) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}
	
	
	
}
