package daojpa;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Juiz;

public class DAOJuiz extends DAO<Juiz> {

	public DAOJuiz () {}
	
	public Juiz read(Object chave) {
		try{
			String cpf = (String) chave;
			Query q = manager.createQuery("select j from Juiz j where j.cpf= '" + cpf +"'");
			return (Juiz) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}

	
	
	
}
