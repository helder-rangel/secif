package daojpa;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Candidato;

public class DAOCandidato extends DAO<Candidato> {

	public DAOCandidato () {
		
	}


	public Candidato read(Object chave) {
		try{
			String matricula = (String) chave;
			Query q = manager.createQuery("select c from Candidato c where c.matricula= '" + matricula +"'");
			return (Candidato) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}
	
//	public List<Candidato> buscarPorCurso (String curso) {
//		Query q = manager.query();
//		q.constrain(Candidato.class);
//		q.descend("curso").constrain(curso);
//		List<Candidato> resultado = q.execute(); 
//		return resultado;
//	}
//	
//	public Candidato buscarCandidato(Candidato candidato) {
//		Query q = manager.query();
//		q.constrain(Candidato.class);
//		q.equals(candidato);
//		Candidato resultado = (Candidato) q.execute();		
//		return resultado;
//	}
//	
//	public void buscarAtividades(String matricula, LocalDate data) {
//		//TODO
//	}
	
}
