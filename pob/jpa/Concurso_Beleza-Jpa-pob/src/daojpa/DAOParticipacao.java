/**IFPB - Curso SI - Disciplina de PERSISTENCIA DE OBJETOS
 * @author Prof Fausto Ayres
 */

package daojpa;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Participacao;


public class DAOParticipacao extends DAO<Participacao>{
	public DAOParticipacao(){
		super();
	}

	public Participacao read (Object chave){
		try{

			boolean checkin = (Boolean) chave;
			Query q = manager.createQuery("select p from Participacao p where p.checkin = :n");
            q.setParameter("n", chave);
			return (Participacao) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}
	
//  //pode-se sobrescrever o metodo readAll da classe DAO para ordenar o resultado 
//	public List<Pessoa> readAll(){
//		Query query = manager.createQuery("select p from Pessoa p order by p.id");
//		return (List<Pessoa>) query.getResultList();
//	}




}

