package aplicacaoswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;

public class AtividadeLista extends JFrame {

	private JPanel contentPane;
//	private JTextArea textArea;
	private JButton btnListarAtividades;

	/**
	 * Create the application.
	 */
	public AtividadeLista() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Atividade | Listar Atividades");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextArea textArea = new JTextArea();
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(30,38,396,106);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scroll);
		
		btnListarAtividades = new JButton("Listar atividades");
		btnListarAtividades.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					textArea.setText(Fachada.listarAtividades());
				}
				catch(Exception erro) {
					JOptionPane.showMessageDialog(null,erro.getStackTrace());
				}
			}
		});
		
		btnListarAtividades.setBounds(152, 205, 163, 25);
		contentPane.add(btnListarAtividades);
	}
}
