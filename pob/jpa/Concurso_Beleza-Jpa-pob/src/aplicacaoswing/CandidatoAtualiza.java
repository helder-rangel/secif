package aplicacaoswing;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fachada.Fachada;
import modelo.Candidato;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CandidatoAtualiza extends JFrame {

	private JPanel contentPane;
	private JTextField txtCandidatomatricula;
	private JTextField txtNovamatricula;
	
	/**
	 * Create the application.
	 */
	public CandidatoAtualiza() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Candidato | Atualizar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblCandidato = new JLabel("Matrícula");
		lblCandidato.setBounds(23, 12, 105, 15);
		getContentPane().add(lblCandidato);
		
		txtCandidatomatricula = new JTextField();
		txtCandidatomatricula.setText("candidatoMatricula");
		txtCandidatomatricula.setBounds(23, 39, 114, 19);
		getContentPane().add(txtCandidatomatricula);
		txtCandidatomatricula.setColumns(10);
		
		JLabel lblNovaMatrcula = new JLabel("Nova matrícula");
		lblNovaMatrcula.setBounds(23, 97, 114, 15);
		getContentPane().add(lblNovaMatrcula);
		
		txtNovamatricula = new JTextField();
		txtNovamatricula.setText("novaMatricula");
		txtNovamatricula.setBounds(23, 124, 114, 19);
		getContentPane().add(txtNovamatricula);
		txtNovamatricula.setColumns(10);
		
		JButton btnAtualizarCandidato = new JButton("Atualizar candidato");
		btnAtualizarCandidato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String matricula = txtCandidatomatricula.getText();
				String matNova = txtNovamatricula.getText();
				try {
					Candidato atualCand = Fachada.atualizarMatriculaCandidato(matricula,matNova);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnAtualizarCandidato.setBounds(12, 165, 180, 25);
		getContentPane().add(btnAtualizarCandidato);
		
	}

}
