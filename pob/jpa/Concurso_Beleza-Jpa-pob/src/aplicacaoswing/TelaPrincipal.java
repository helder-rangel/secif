package aplicacaoswing;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import fachada.Fachada;

public class TelaPrincipal {
	// itens do menu
	private JFrame frmPrincipal;
	private JMenuItem mntmCadastrar;
	private JMenuItem mntmAtualizar;
	private JMenuItem mntmListar;
	private JMenuItem mntmListar_4;
	private JMenuItem mntmListar_3;
	private JMenuItem mntmListar_2;
	private JMenuItem mntmApagar;
	private JMenuItem mntmApagar_1;

	// o menu	
	private JMenu mnAtividade;
	private JMenu mnCandidato;
	private JMenu mnEvento;
	private JMenu mnJuiz;
	private JMenu mnJulgamento;
	private JMenu mnInscricao;
	private JMenuItem mntmListar_1;
	private JMenuItem mnBuscas;
	private JMenuItem mnBuscas_1;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal window = new TelaPrincipal();
					window.frmPrincipal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TelaPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPrincipal = new JFrame();
		frmPrincipal.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				Fachada.inicializar();
				JOptionPane.showMessageDialog(null, "sistema inicializado !");
			}
			@Override
			public void windowClosing(WindowEvent e) {
				Fachada.finalizar();
				JOptionPane.showMessageDialog(null, "sistema finalizado !");
			}
		});
		frmPrincipal.setTitle("Gerenciador de eventos");
		frmPrincipal.setBounds(100, 100, 528, 300);
		frmPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrincipal.getContentPane().setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		frmPrincipal.setJMenuBar(menuBar);

		mnAtividade = new JMenu("Atividade");
		menuBar.add(mnAtividade);

		mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtividadeCadastro j = new AtividadeCadastro();
				j.setVisible(true);
			}

		});
		mnAtividade.add(mntmCadastrar);

		mntmListar = new JMenuItem("Listar");
		mntmListar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtividadeLista j = new AtividadeLista();
				j.setVisible(true);
			}
		});
		mnAtividade.add(mntmListar);

		mntmAtualizar = new JMenuItem("Atualizar");
		mntmAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtividadeAtualiza j = new AtividadeAtualiza();
				j.setVisible(true);
			}
		});
		mnAtividade.add(mntmAtualizar);		


		mntmApagar_1 = new JMenuItem("Apagar");
		mntmApagar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AtividadeApaga j = new AtividadeApaga();
				j.setVisible(true);
			}
		});
		mnAtividade.add(mntmApagar_1);
		
		mnBuscas = new JMenuItem("Buscas");
		mnBuscas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AtividadeBuscas j = new AtividadeBuscas();
			}
		});
		mnAtividade.add(mnBuscas);

		mnCandidato = new JMenu("Candidato");
		menuBar.add(mnCandidato);

		mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CandidatoCadastro j = new CandidatoCadastro();
				j.setVisible(true);
			}

		});
		mnCandidato.add(mntmCadastrar);

		mntmAtualizar = new JMenuItem("Atualizar");
		mntmAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CandidatoAtualiza j = new CandidatoAtualiza();
				j.setVisible(true);
			}
		});
		mnCandidato.add(mntmAtualizar);


		mntmApagar = new JMenuItem("Apagar");
		mntmApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CandidatoApaga j = new CandidatoApaga();
				j.setVisible(true);
			}
		});
		mnCandidato.add(mntmApagar);
		
		mntmListar_1 = new JMenuItem("Listar");
		mntmListar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CandidatoLista j = new CandidatoLista();
				j.setVisible(true);
			}
		});
		mnCandidato.add(mntmListar_1);
		
		mnBuscas_1 = new JMenuItem("Buscas");
		mnBuscas_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CandidatoBusca j = new CandidatoBusca();
			}
		});
		mnCandidato.add(mnBuscas_1);
		

		mnEvento = new JMenu("Evento");
		menuBar.add(mnEvento);

		mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventoCadastra j = new EventoCadastra();
				j.setVisible(true);
			}

		});
		mnEvento.add(mntmCadastrar);

		mntmListar_2 = new JMenuItem("Listar");
		mntmListar_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventoLista j = new EventoLista();
				j.setVisible(true);
			}
		});
		mnEvento.add(mntmListar_2);		


		mntmAtualizar = new JMenuItem("Atualizar");
		mntmAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventoAtualiza j = new EventoAtualiza();
				j.setVisible(true);
			}
		});
		mnEvento.add(mntmAtualizar);

		mntmApagar = new JMenuItem("Apagar");
		mntmApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventoApaga j = new EventoApaga();
				j.setVisible(true);
			}
		});
		mnEvento.add(mntmApagar);
		
		JMenu mnInscricao_1 = new JMenu("Inscricao");
		menuBar.add(mnInscricao_1);
		
		JMenuItem mntmCadastrar_1 = new JMenuItem("Cadastrar");
		mntmCadastrar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscricaoCadastra j = new InscricaoCadastra();
				j.setVisible(true);
			}
		});
		mnInscricao_1.add(mntmCadastrar_1);
		
		JMenuItem mntmListar_5 = new JMenuItem("Listar");
		mntmListar_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscricaoLista j = new InscricaoLista();
			}
		});
		mnInscricao_1.add(mntmListar_5);
		
		JMenuItem mntmAtualizar_1 = new JMenuItem("Atualizar");
		mntmAtualizar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscricaoAtualiza j = new InscricaoAtualiza();
				j.setVisible(true);
			}
		});
		mnInscricao_1.add(mntmAtualizar_1);
		
		JMenuItem mntmApagar_2 = new JMenuItem("Apagar");
		mntmApagar_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InscricaoApaga j = new InscricaoApaga();
				j.setVisible(true);
			}
		});
		mnInscricao_1.add(mntmApagar_2);

		mnJuiz = new JMenu("Juiz");
		menuBar.add(mnJuiz);

		mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JuizCadastra j = new JuizCadastra();
				j.setVisible(true);
			}

		});
		mnJuiz.add(mntmCadastrar);

		mntmListar_3 = new JMenuItem("Listar");
		mntmListar_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JuizLista j = new JuizLista();
				j.setVisible(true);
			}
		});
		mnJuiz.add(mntmListar_3);		


		mntmAtualizar = new JMenuItem("Atualizar");
		mntmAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JuizAtualiza j = new JuizAtualiza();
				j.setVisible(true);
			}
		});
		mnJuiz.add(mntmAtualizar);

		mntmApagar = new JMenuItem("Apagar");
		mntmApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JuizApaga j = new JuizApaga();
				j.setVisible(true);
			}
		});
		mnJuiz.add(mntmApagar);

		mnJulgamento = new JMenu("Julgamento");
		menuBar.add(mnJulgamento);

		mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JulgamentoCadastra j = new JulgamentoCadastra();
				j.setVisible(true);
			}
		});
		mnJulgamento.add(mntmCadastrar);

		mntmListar_4 = new JMenuItem("Listar");
		mntmListar_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JulgamentoLista j = new JulgamentoLista();
			}
		});
		mnJulgamento.add(mntmListar_4);		


		mntmAtualizar = new JMenuItem("Atualizar");
		mntmAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JulgamentoCadastra j = new JulgamentoCadastra();
				j.setVisible(true);
			}
		});
		mnJulgamento.add(mntmAtualizar);

		mntmApagar = new JMenuItem("Apagar");
		mntmApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JulgamentoCadastra j = new JulgamentoCadastra();
				j.setVisible(true);
			}
		});
		mnJulgamento.add(mntmApagar);

		mnInscricao = new JMenu("Julgamento");
		menuBar.add(mnJulgamento);

		mntmCadastrar = new JMenuItem("Cadastrar");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscricaoCadastra j = new InscricaoCadastra();
				j.setVisible(true);
			}

		});
		mnInscricao.add(mntmCadastrar);

		mntmListar = new JMenu("Listar");
		mntmListar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscricaoLista j = new InscricaoLista();
				j.setVisible(true);
			}
		});
		mnInscricao.add(mntmListar);		


		mntmAtualizar = new JMenuItem("Atualizar");
		mntmAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscricaoAtualiza j = new InscricaoAtualiza();
				j.setVisible(true);
			}
		});
		mnInscricao.add(mntmAtualizar);

		mntmApagar = new JMenuItem("Apagar");
		mntmApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InscricaoApaga j = new InscricaoApaga();
				j.setVisible(true);
			}
		});
		mnInscricao.add(mntmApagar);

	}
}
