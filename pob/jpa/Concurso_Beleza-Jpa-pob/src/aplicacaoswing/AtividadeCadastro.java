package aplicacaoswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Atividade;

public class AtividadeCadastro extends JFrame {

	private JFrame frmCadastro;
	private JLabel label1;
	private JTextField textFieldNomeAtividade;
	private JTextField jtextfielddescricao;
	//private JTextField jtextfieldMinimoParticipantes;
	private JTextField jtextfieldMaxParticipantes;
	private JLabel lblDescricao;
	private JLabel lblHorrio;
	private JTextField textFieldHorario;
	private JTextField textFieldDescricao;
	private JButton btnCadastrarAtividade;
	private JTextField jtextfieldMinimoParticipantes;
	private JTextField jtextfieldMaximoParticipantes;
	private JTextField txtHorrio;
	


	/**
	 * Create the application.
	 */
	public AtividadeCadastro() {
		initialize();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmCadastro = new JFrame();
		frmCadastro.setTitle("Atividade | Cadastrar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(26, 12, 70, 15);
		getContentPane().add(lblNome);
		
		textFieldNomeAtividade = new JTextField();
		textFieldNomeAtividade.setBounds(46, 27, 114, 19);
		getContentPane().add(textFieldNomeAtividade);
		textFieldNomeAtividade.setColumns(10);
		
		JLabel lblAno = new JLabel("Descrição");
		lblAno.setBounds(26, 52, 70, 15);
		getContentPane().add(lblAno);
		
		textFieldDescricao = new JTextField();
		textFieldDescricao.setBounds(46, 67, 114, 19);
		getContentPane().add(textFieldDescricao);
		textFieldDescricao.setColumns(10);
		
		jtextfieldMinimoParticipantes = new JTextField();
		jtextfieldMinimoParticipantes.setText("número mínimo");
		jtextfieldMinimoParticipantes.setBounds(46, 98, 114, 19);
		getContentPane().add(jtextfieldMinimoParticipantes);
		jtextfieldMinimoParticipantes.setColumns(10);
		
		JLabel lblMnimoDeParticipantes = new JLabel("Mínimo de participantes");
		lblMnimoDeParticipantes.setBounds(26, 82, 177, 15);
		getContentPane().add(lblMnimoDeParticipantes);
		
		jtextfieldMaximoParticipantes = new JTextField();
		jtextfieldMaximoParticipantes.setText("número máximo");
		jtextfieldMaximoParticipantes.setBounds(46, 142, 114, 19);
		getContentPane().add(jtextfieldMaximoParticipantes);
		jtextfieldMaximoParticipantes.setColumns(10);
		
		JLabel lblMximoDeParticipantes = new JLabel("Máximo de participantes");
		lblMximoDeParticipantes.setBounds(26, 126, 189, 15);
		getContentPane().add(lblMximoDeParticipantes);

		txtHorrio = new JTextField();
		txtHorrio.setText("dd/mm/aa");
		txtHorrio.setBounds(46, 192, 114, 19);
		getContentPane().add(txtHorrio);
		txtHorrio.setColumns(10);
		
		
		JButton btnCadastrarAtividade = new JButton("Cadastrar atividade");
		btnCadastrarAtividade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = textFieldNomeAtividade.getText();
				String descricao = textFieldDescricao.getText();
				double min = Double.parseDouble(jtextfieldMinimoParticipantes.getText());
				double max = Double.parseDouble(jtextfieldMaximoParticipantes.getText());
				DateTimeFormatter formatado = DateTimeFormatter.ofPattern("dd/MM/yy");
				LocalDate horario;
				String hora = txtHorrio.getText();
				try {
					horario = LocalDate.parse(hora,formatado);
					Atividade cadastro = Fachada.cadastrarAtividade(nome,descricao,min,max,horario);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCadastrarAtividade.setBounds(26, 236, 177, 25);
		getContentPane().add(btnCadastrarAtividade);
		
		
		JLabel lblHorrio_1 = new JLabel("Horário");
		lblHorrio_1.setBounds(26, 173, 70, 15);
		getContentPane().add(lblHorrio_1);
		
		
		//mostrar a janela
		frmCadastro.setVisible(true);
	}
}
