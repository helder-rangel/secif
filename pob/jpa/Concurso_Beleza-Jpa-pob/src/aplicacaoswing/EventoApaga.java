package aplicacaoswing;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fachada.Fachada;
import modelo.Evento;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EventoApaga extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	
	/**
	 * Create the application.
	 */
	public EventoApaga() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {

		setTitle("Evento | Apagar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(28, 28, 70, 15);
		getContentPane().add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(28, 55, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JButton btnApagarEvento = new JButton("Apagar evento");
		btnApagarEvento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = txtNome.getText();
				try {
					Evento apagEvent = Fachada.apagarEvento(nome);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnApagarEvento.setBounds(28, 86, 148, 25);
		getContentPane().add(btnApagarEvento);
		
		JTextArea txtrResultadoapagarevento = new JTextArea();
		txtrResultadoapagarevento.setText("resultadoApagarEvento");
		txtrResultadoapagarevento.setBounds(26, 264, 139, 29);
		getContentPane().add(txtrResultadoapagarevento);
		
	}

}
