package modelo;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Juiz")
public class Juiz extends Pessoa {

	private boolean externo;
	private String instituicao;
	
	@OneToMany(mappedBy="juiz", cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
	private ArrayList<Julgamento> julgamento;
	
	public Juiz() {
		super();
		this.julgamento = new ArrayList<Julgamento>();
	}

	
	public Juiz(String nome, String email, String cpf, String telefone, boolean externo, String instituicao) {
		super(nome, email, cpf, telefone);
		this.externo = externo;
		this.instituicao = instituicao;
		this.julgamento = new ArrayList<Julgamento>();
	}

	public boolean isExterno() {
		return externo;
	}


	public String getInstituicao() {
		return instituicao;
	}


	public ArrayList<Julgamento> getJulgamento() {
		return julgamento;
	}


	public void setExterno(boolean externo) {
		this.externo = externo;
	}


	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}


	public void setJulgamento(ArrayList<Julgamento> julgamento) {
		this.julgamento = julgamento;
	}
	
	@Override
	public String toString() {
		return "Juiz [externo=" + externo + ", instituicao=" + instituicao + ", julgamento=" + julgamento
				+ ", Pessoa" + super.toString() + "]";
	}
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (externo ? 1231 : 1237);
		result = prime * result + ((instituicao == null) ? 0 : instituicao.hashCode());
		result = prime * result + ((julgamento == null) ? 0 : julgamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Juiz other = (Juiz) obj;
		if (externo != other.externo)
			return false;
		if (instituicao == null) {
			if (other.instituicao != null)
				return false;
		} else if (!instituicao.equals(other.instituicao))
			return false;
		if (julgamento == null) {
			if (other.julgamento != null)
				return false;
		} else if (!julgamento.equals(other.julgamento))
			return false;
		return true;
	}


	public void adicionarJulgamento(Julgamento julgamento) throws Exception {
		
		for (Julgamento j : this.julgamento) {
			if (j.equals(julgamento)) {
				throw new Exception("Não e possivel inserir: julgamento ja existente");
			}
		}
		
		this.julgamento.add(julgamento);
		
	}

	public void alterarJulgamento(Julgamento julgamentoAntigo, Julgamento julgamentoNovo ) throws Exception {
		
		Julgamento julgamentoTemporario = null;
		
		for (Julgamento j : this.julgamento) {
			if (j.equals(julgamentoAntigo)) {
				julgamentoTemporario = j;
				this.julgamento.remove(j);
			}
		}
		
		if (julgamentoTemporario == null) {
			throw new Exception("Não e possivel alterar: julgamento nao existente");
		}
		
		this.julgamento.add(julgamentoNovo);		
	}
	
	public void removerJulgamento(Julgamento julgamento) throws Exception {
		
		for (Julgamento j : this.julgamento) {
			if (j.equals(julgamento)) {
				this.julgamento.remove(julgamento);
			}
		}
		
		throw new Exception("Não e possivel remover: julgamento nao existente");
		
	}
	
	public String listarJulgamento() {
		
		String retorno = "Listagem dos Julgamentos\n";
		
		for (Julgamento j : this.julgamento) {
			retorno+= j +"\n";
		}
		
		return retorno;
	}
	
	public Julgamento procurarJulgamento(Julgamento julgamento) throws Exception {
		
		for (Julgamento j : this.julgamento) {
			if (j.equals(julgamento)) {
				return j;
			}
		}
		
		throw new Exception("Nao e possivel coletar: julgamento nao existente");
	}
	
}
