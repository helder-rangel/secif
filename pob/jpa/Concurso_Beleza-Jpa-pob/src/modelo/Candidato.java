package modelo;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Candidato")
public class Candidato extends Pessoa {
	
	@Id		
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String curso;
	private String matricula;
	
	@OneToMany(mappedBy="candidato", cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
	private ArrayList<Inscricao> inscricao;
	
	
	public Candidato() {
		super();
		this.inscricao = new ArrayList<Inscricao>();
	}

	public Candidato(String nome, String email, String cpf, String telefone, String curso, String matricula) {
		super(nome, email, cpf, telefone);
		this.curso = curso;
		this.matricula = matricula;
		this.inscricao = new ArrayList<Inscricao>();
	}

	public String getCurso() {
		return curso;
	}

	public String getMatricula() {
		return matricula;
	}

	public ArrayList<Inscricao> getInscricao() {
		return inscricao;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public void setInscricao(ArrayList<Inscricao> inscricao) {
		this.inscricao = inscricao;
	}

	@Override
	public String toString() {
		return "Candidato [curso=" + curso + ", matricula=" + matricula + ", inscricao=" + inscricao + ", Pessoa="
				+ super.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((curso == null) ? 0 : curso.hashCode());
		result = prime * result + ((inscricao == null) ? 0 : inscricao.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Candidato other = (Candidato) obj;
		if (curso == null) {
			if (other.curso != null)
				return false;
		} else if (!curso.equals(other.curso))
			return false;
		if (inscricao == null) {
			if (other.inscricao != null)
				return false;
		} else if (!inscricao.equals(other.inscricao))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}

	public void adicionarInscricao(Inscricao inscricao) throws Exception {
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(inscricao)) {
				throw new Exception("Não e possivel inserir: inscricao ja existente");
			}
		}
		
		this.inscricao.add(inscricao);		
	}
	
	public void alterarInscricao(Inscricao inscricaoAntigo, Inscricao inscricaoNovo ) throws Exception {
		
		Inscricao inscricaoTemporario = null;
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(inscricaoAntigo)) {
				inscricaoTemporario = i;
				this.inscricao.remove(i);
			}
		}
		
		if (inscricaoTemporario == null) {
			throw new Exception("Não e possivel alterar: inscricao nao existente");
		}
		
		this.inscricao.add(inscricaoNovo);		
	}
	
	public void removerInscricao(Inscricao inscricao) throws Exception {
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(inscricao)) {
				this.inscricao.remove(inscricao);
			}
		}
		
		throw new Exception("Não e possivel remover: inscricao nao existente");		
	}
	
	public String listarInscricao() {
		
		String retorno = "Listagem das Inscricoes\n";
		
		for (Inscricao i: this.inscricao) {
			retorno+= i + "\n";
		}
		
		return retorno;
	}
	
	public Inscricao procurarInscricao(Inscricao inscricao) throws Exception {
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(inscricao)) {
				return i;
			}
		}
		
		throw new Exception("Nao e possivel remover: inscricao nao existente");		
	}
	
		
}