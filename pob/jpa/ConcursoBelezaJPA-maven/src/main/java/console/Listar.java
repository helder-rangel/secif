package console;

import fachada.Fachada;

public class Listar {

	public static void main (String[] args) {
		try {
			listar();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public static void listar() {

		Fachada.inicializar();
		
		System.out.println("Listando todos os objetos do banco");

		String atividades = Fachada.listarAtividades();
		System.out.println(atividades);

		String candidatos = Fachada.listarCandidatos();
		System.out.println(candidatos);

		String eventos = Fachada.listarEventos();
		System.out.println(eventos);

		String inscricoes = Fachada.listarInscricoes();
		System.out.println(inscricoes);			

		String juizes = Fachada.listarJuizes();
		System.out.println(juizes);

		String julgamentos = Fachada.listarJulgamentos();
		System.out.println(julgamentos);

		String participacoes = Fachada.listarParticipacoes();
		System.out.println(participacoes);

		String pessoas = Fachada.listarPessoas();
		System.out.println(pessoas);
		
		Fachada.finalizar();

	}

	
	
}
