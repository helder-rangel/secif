package console;

import modelo.*;
import fachada.Fachada;

import java.time.LocalDate;
import java.util.ArrayList;

public class Cadastrar {

	public static void main (String[] args) {
		try {
			cadastrar();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public static void cadastrar(){

		Atividade atividade1,atividade2,atividade3;
		Candidato candidato1,candidato2,candidato3;
		Evento evento1,evento2,evento3;
		Inscricao inscricao1,inscricao2,inscricao3;
		Juiz juiz1,juiz2,juiz3;
		Julgamento julgamento;
		Participacao participacao1,participacao2,participacao3;
		Pessoa pessoa1,pessoa2,pessoa3;
		ArrayList<Julgamento> julgamentos = new ArrayList<Julgamento>();
		
		Fachada.inicializar();

		System.out.println("cadastrando todos os objetos...");

		try {
			julgamento=Fachada.cadastrarJulgamento(4.0,"Desenvoltura");
			julgamentos.add(julgamento);
			julgamento=Fachada.cadastrarJulgamento(6.1,"Beleza");
			julgamentos.add(julgamento);
			julgamento=Fachada.cadastrarJulgamento(9.1,"Simpatia");
			julgamentos.add(julgamento);

			atividade1=Fachada.cadastrarAtividade("Beleza","Concurso de beleza do secif 2019",1,256,LocalDate.now());
			atividade2=Fachada.cadastrarAtividade("Volei","Competicao de volei do secif 2019",1,256,LocalDate.now());
			atividade3=Fachada.cadastrarAtividade("Futebol de sal�o ","Competicao de futsal do secif 2019",5,15,LocalDate.now());
			atividade3=Fachada.buscarAtividade(new Atividade("Futebol de sal�o ","Competicao de futsal do secif 2019",5,15,LocalDate.now()) );
			System.out.println(atividade3);

			candidato1=Fachada.cadastrarCandidato("Jose Xico","jose.xico@email.com.br","9809889808","988776655","Eletrotecnica","20131720580");
			candidato2=Fachada.cadastrarCandidato("Maria Josefina ","MariaJosefina@email.com.br","9809812318","988776455","Edificacoes","20181920580");
			candidato3=Fachada.cadastrarCandidato("Marcos Neto ","marcos.nett@email.com.br","9809812218","9487764312","Controle Ambiental","20161960510");
			System.out.println(candidato2);

			inscricao1=Fachada.cadastrarInscricao(atividade1,false,candidato2,LocalDate.now() );
			inscricao2=Fachada.cadastrarInscricao(atividade2,false,candidato1,LocalDate.now() );
			inscricao3=Fachada.cadastrarInscricao(atividade3,false,candidato3,LocalDate.now() );
			System.out.println(inscricao1);

		} catch (Exception e) {
			e.printStackTrace() ;
		}


		try {
			evento1=Fachada.cadastrarEvento("SECIT","2019");
			evento2=Fachada.cadastrarEvento("Secif","2019");
			evento3=Fachada.cadastrarEvento("If.Tech","2019");
			System.out.println(evento1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			juiz1=Fachada.cadastrarJuiz("Beltrano","bletrano@email.com","312.421.786-21","88888888",true,"Pio XI");
			juiz2=Fachada.cadastrarJuiz("Sicrano","sicrano@email.com","333.112.231.99","88888888",false,"IFPB - Jo�o Pessoa");
			juiz3=Fachada.cadastrarJuiz("Antonio Silva Sales","toinss@email.com","120.212.900-00","88888888",true,"UFPB - Campus V");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		try {
			participacao1=Fachada.cadastrarParticipacao(false,false,0);
			participacao2=Fachada.cadastrarParticipacao(true,true,10);
//			participacao3=Fachada.cadastrarParticipacao(true,false,5); // TODO -> Deu erro pois j� existia, necess�rio checar modelagem
			System.out.println(participacao1);

			pessoa1=Fachada.cadastrarPessoa("joao","asa@aaa","9809889808","988776655");
			pessoa2=Fachada.cadastrarPessoa("Tayna Sales","tata@email.com","9987543808","999279655");
			pessoa3=Fachada.cadastrarPessoa("Xico Cesar","xico.czr@aaa","9123489808","999772255");	
			System.out.println(pessoa1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Fachada.finalizar();
	}	
	
}
