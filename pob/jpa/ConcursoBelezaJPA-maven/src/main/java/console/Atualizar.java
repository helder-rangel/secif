package console;

import java.time.LocalDate;
import java.util.ArrayList;

import fachada.Fachada;
import modelo.*;

public class Atualizar {

	public static void main (String[] args) {
		try {
			atualizar();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public static void atualizar()  throws Exception{

		Atividade atividade,atividade2,atividade3;
		Candidato candidato,candidato3;
		Evento evento;
		Inscricao inscricao;
		Juiz juiz;
		Julgamento julgamento;
		Participacao participacao;
		Pessoa pessoa;

		Fachada.inicializar();

		System.out.println("Atualizando o nome de joao para Josefino...");
		//ATUALIZAR OBJETOS | UPDATE OBJECTS

		 Fachada.atualizarNomePessoa("Xico Cesar", "Xico C�sar"); 

		 Fachada.atualizarEmailPessoa("Xico Cesar", "xico.czr@aaa", "xico.czr@email.com"); 

		 Fachada.atualizarTelefonePessoa("Xico Cesar", "999772255", "991772255"); 

		 Fachada.atualizarCpfPessoa("Xico Cesar", "9123489808", "9923489800"); 


		 Fachada.atualizarMatriculaCandidato("20161960510", "20161960511"); 
			
		 Fachada.atualizarCursoCandidato("20161960511", "Controle Ambiental", "Controle_Ambiental"); 
			

		 Fachada.atualizarInstituicaoJuiz("120.212.900-00", "UFPB - Campus V", "IFPB - Jo�o Pessoa"); 
			
		 Fachada.atualizarExternoJuiz("120.212.900-00", true, false);
			   
			
		 Fachada.atualizarNomeAtividade ("Volei", "Voleibol");
			
		 Fachada.atualizarDescricaoAtividade ("Voleibol" ,"Competicao de voleibol do secif 2019"); 
			
		 Fachada.atualizarNumMaxAtividade ("Voleibol" ,6); 
			
		 Fachada.atualizarNumMinAtividade ("Voleibol" ,6); 
			
		 Fachada.atualizarHorarioAtividade ("Voleibol" ,LocalDate.of(2019, 9, 15) ); 
			
		 ArrayList<Julgamento> julgamentos1 = new ArrayList<Julgamento>();
		 julgamentos1.add(new Julgamento(7,"Jogo1"));
		 Fachada.atualizarListaJulgamentosAtividade ("Voleibol" ,  julgamentos1 );
			
			 
			
		 Fachada.atualizarNomeEvento ("If.Tech", "If_Tech"); 
			
		 Fachada.atualizarAnoEvento ("If_Tech", "2019", "2019.2");
			
		 atividade2=Fachada.buscarAtividadePeloNome("Voleibol");
		 atividade3=Fachada.buscarAtividadePeloNome("Futebol de sal�o ");
		 candidato3=Fachada.buscarCandidatoPelaMatricula("20161960510");
		 Fachada.atualizarCompeticaoInscricao (candidato3, atividade3, atividade2); 
			  
			
		 Fachada.atualizarCertificadoInscricao (candidato3, false, true); 
			
		 Fachada.atualizarDataInscricao (candidato3, LocalDate.of(2019, 9, 5)); 
			
			
		 Fachada.atualizarQuesitoJulgamento("Beleza", "Beleza_no_Palco");
			
		 Fachada.atualizarNotaJulgamento("Beleza_no_Palco", 7); 
			
			
		 Fachada.atualizarCheckinParticipacao (true, false);
			
		 Fachada.atualizarCertificadoParticipacao (false, false);   
			
		 Fachada.atualizarPontuacaoParticipacao (false, 2);   
		
		 Fachada.finalizar();
	}	

	
}
