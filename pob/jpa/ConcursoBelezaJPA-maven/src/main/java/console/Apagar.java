package console;

import fachada.Fachada;
import modelo.Candidato;

public class Apagar {

	public static void main (String[] args) {
		try {
			apagar();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}

	public static void apagar()  throws Exception{

		Fachada.inicializar();

		Candidato candidato = Fachada.buscarCandidatoPelaMatricula("20131720580");

		Fachada.apagarAtividade("Futebol de sal�o ");
		Fachada.apagarEvento("SECIT");
		Fachada.apagarInscricao(candidato);//� necess�rio passar um objeto do tipo candidato
		Fachada.apagarCandidato("20131720580");
		Fachada.apagarJuiz("312.421.786-21"); 
		Fachada.apagarJulgamento("Desenvoltura");
		Fachada.apagarParticipacao(false);
		Fachada.apagarPessoa("joao");
		
		Fachada.finalizar();
	}	


}
