/**IFPB - Curso SI - Disciplina de PERSISTENCIA DE OBJETOS
 * @author Prof Fausto Ayres
 */

package daojpa;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Pessoa;


public class DAOPessoa extends DAO<Pessoa>{
	public DAOPessoa(){
		super();
	}

	public Pessoa read (Object chave){
		try{
			String nome = (String) chave;
			Query q = manager.createQuery("select p from Pessoa p where p.nome= '" + nome +"'");
			return (Pessoa) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}
	
//  //pode-se sobrescrever o metodo readAll da classe DAO para ordenar o resultado 
//	public List<Pessoa> readAll(){
//		Query query = manager.createQuery("select p from Pessoa p order by p.id");
//		return (List<Pessoa>) query.getResultList();
//	}




}

