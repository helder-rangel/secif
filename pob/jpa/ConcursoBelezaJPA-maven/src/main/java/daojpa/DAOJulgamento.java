package daojpa;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Julgamento;


public class DAOJulgamento extends DAO<Julgamento> {

	public DAOJulgamento() {}
	
	public Julgamento read(Object chave) {
		try{
			String quesito = (String) chave;
			Query q = manager.createQuery("select j from Julgamento j where j.quesito = '" + quesito +"'");
			return (Julgamento) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}

}
