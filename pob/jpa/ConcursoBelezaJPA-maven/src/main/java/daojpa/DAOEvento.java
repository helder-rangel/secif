package daojpa;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Evento;

public class DAOEvento extends DAO<Evento> {

	public DAOEvento () {
		
	}

	public Evento read(Object chave) {
		try{
			String nome = (String) chave;
			Query q = manager.createQuery("select e from Evento e where e.nome= '" + nome +"'");
			return (Evento) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}
	
	
	
}
