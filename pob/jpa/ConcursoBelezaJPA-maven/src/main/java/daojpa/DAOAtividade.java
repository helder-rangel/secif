package daojpa;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import modelo.Atividade;

public class DAOAtividade extends DAO<Atividade> {

	public DAOAtividade () {
		
	}


	public Atividade read(Object chave) {
		try{
			String nome = (String) chave;
			Query q = manager.createQuery("select a from Atividade a where a.nome= '" + nome +"'");
			return (Atividade) q.getSingleResult();

		}catch(NoResultException e){
			return null;
		}
	}
	
//	public Atividade buscarPeloNomeEEvento(String nomeAtividade, String nomeEvento ) {
//		String nome = (String) nomeAtividade;
//		Query q = manager.query();
//		q.constrain(Atividade.class);
//		q.descend("nome").constrain(nome).and(
//			q.descend("evento").descend("nome").constrain(nomeEvento));
//		List<Atividade> resultados = q.execute();
//		if (resultados.size()>0)
//			return resultados.get(0);
//		else
//			return null;
//	}
//	
//	public List<String> buscarParticipantes(String nomeAtividade, String nomeEvento) {
//		Atividade atividade = buscarPeloNomeEEvento(nomeAtividade,nomeEvento);
//		Query q = manager.query();
//		q.constrain(Atividade.class);
//		 if (q.equals(atividade) ) {
//			  q.descend("participacao").descend("inscricao").descend("candidato").descend("nome");
//			  List<String> resultados = q.execute();
//			  return resultados;
//		 } else 
//			 	return null;
//		
//	}
//	
//	public List<Atividade> buscarAtividadesPeloDia(LocalDate dia){
//		Query q = manager.query();
//		q.constrain(Atividade.class);
//		q.descend("horario").constrain(dia);
//		List<Atividade> atividades = q.execute();
//		if (atividades.size() > 0) {
//			return atividades;
//		} 
//		else 
//			return null;		
//	}
//	
//	public List<String> buscarJulgadores(String nomeAtividade, String nomeEvento) {
//		Atividade atividade = buscarPeloNomeEEvento(nomeAtividade,nomeEvento);
//		Query q = manager.query();
//		q.constrain(Atividade.class);
//		 if (q.equals(atividade) ) {
//			  q.descend("pontuacao").descend("juiz").descend("nome");
//			  List<String> resultados = q.execute();
//			  return resultados;
//		 } else 
//			 	return null;
//		
//	}
	
}
