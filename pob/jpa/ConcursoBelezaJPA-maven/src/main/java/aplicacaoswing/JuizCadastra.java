package aplicacaoswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachada.Fachada;

public class JuizCadastra extends JFrame {

	private JPanel contentPane;
	private JFrame frmCadastro;
	private JTextField textFieldNome;
	private JTextField textFieldEmail;
	private JTextField textFieldCpf;
	private JTextField textFieldFone;
	private JTextField textFieldExterno;
	private JTextField textFieldInstituicao;
	
	
	/**
	 * Create the application.
	 */
	public JuizCadastra() {
		initialize();
	}

	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Juiz | Cadastrar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 70, 15);
		getContentPane().add(lblNome);
		
		textFieldNome = new JTextField();
		textFieldNome.setBounds(71, 12, 114, 19);
		getContentPane().add(textFieldNome);
		textFieldNome.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(12, 39, 70, 15);
		getContentPane().add(lblEmail);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(71, 41, 114, 19);
		getContentPane().add(textFieldEmail);
		textFieldEmail.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(12, 66, 70, 15);
		getContentPane().add(lblCpf);
		
		textFieldCpf = new JTextField();
		textFieldCpf.setBounds(71, 66, 114, 19);
		getContentPane().add(textFieldCpf);
		textFieldCpf.setColumns(10);
		
		JLabel lblFone = new JLabel("Fone");
		lblFone.setBounds(12, 93, 70, 15);
		getContentPane().add(lblFone);
		
		textFieldFone = new JTextField();
		textFieldFone.setBounds(71, 95, 114, 19);
		getContentPane().add(textFieldFone);
		textFieldFone.setColumns(10);
		
		JLabel lblExterno = new JLabel("Externo");
		lblExterno.setBounds(12, 120, 70, 15);
		getContentPane().add(lblExterno);
		
		textFieldExterno = new JTextField();
		textFieldExterno.setBounds(71, 120, 114, 19);
		getContentPane().add(textFieldExterno);
		textFieldExterno.setColumns(10);
		
		JLabel lblInstituicao = new JLabel("Instituição");
		lblInstituicao.setBounds(12, 147, 84, 15);
		getContentPane().add(lblInstituicao);
		
		textFieldInstituicao = new JTextField();
		textFieldInstituicao.setBounds(71, 174, 114, 19);
		getContentPane().add(textFieldInstituicao);
		textFieldInstituicao.setColumns(10);
		
		JButton btnNewButtonCriar = new JButton("Cadastrar Juiz");
		btnNewButtonCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = textFieldNome.getText();
				String email = textFieldEmail.getText();
				String cpf = textFieldCpf.getText();
				String telefone = textFieldFone.getText();
				boolean externo = Boolean.parseBoolean(textFieldExterno.getText());
				String instituicao = textFieldInstituicao.getText();
				
				try {
					Fachada.cadastrarJuiz(nome,email,cpf,telefone,externo,instituicao);
				} catch (Exception juizado)
				{
					JOptionPane.showMessageDialog(null, juizado.getStackTrace());
				}
				
			}
		});
		btnNewButtonCriar.setBounds(27, 219, 158, 25);
		getContentPane().add(btnNewButtonCriar);
		
		JTextArea textAreaResultado = new JTextArea();
		textAreaResultado.setBounds(27, 259, 158, 19);
		getContentPane().add(textAreaResultado);
		setVisible(true);
	}
}
