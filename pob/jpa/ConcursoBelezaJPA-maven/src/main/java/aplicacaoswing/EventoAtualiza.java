package aplicacaoswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Evento;

public class EventoAtualiza extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNomeAtual;
	private JTextField textFieldNovoNome;
	
	/**
	 * Create the application.
	 */
	public EventoAtualiza() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Evento | Atualizar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNomeDoEvento = new JLabel("Nome do evento");
		lblNomeDoEvento.setBounds(12, 12, 175, 15);
		getContentPane().add(lblNomeDoEvento);
		
		textFieldNomeAtual = new JTextField();
		textFieldNomeAtual.setBounds(22, 33, 114, 19);
		getContentPane().add(textFieldNomeAtual);
		textFieldNomeAtual.setColumns(10);
		
		JLabel lblNovoNome = new JLabel("Novo nome");
		lblNovoNome.setBounds(12, 66, 124, 15);
		getContentPane().add(lblNovoNome);
		
		textFieldNovoNome = new JTextField();
		textFieldNovoNome.setBounds(22, 93, 114, 19);
		getContentPane().add(textFieldNovoNome);
		textFieldNovoNome.setColumns(10);
		
		JButton btnAtualizarnome = new JButton("Atualizar nome");
		btnAtualizarnome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeAtual = textFieldNomeAtual.getText();
				String novoNome = textFieldNovoNome.getText();
				try {
					Evento nomeNovo = Fachada.atualizarNomeEvento(nomeAtual,novoNome);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAtualizarnome.setBounds(22, 137, 164, 25);
		getContentPane().add(btnAtualizarnome);
	}

}
