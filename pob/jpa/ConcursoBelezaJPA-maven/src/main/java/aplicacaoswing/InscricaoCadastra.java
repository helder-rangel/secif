package aplicacaoswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Atividade;
import modelo.Candidato;
import modelo.Inscricao;

public class InscricaoCadastra extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldMatricula;
	private JTextField textFieldAnoEvento;
	private JLabel certificado;
	private JTextField txtcompeticao;
	private JTextField txtDdmmyyyy;
	private JButton btnCadastrarEvento;
	private JRadioButton rdbtnSim;
	private JRadioButton rdbtnNao;

	/**
	 * Create the application.
	 */
	public InscricaoCadastra() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {

		setTitle("Incrição | Cadastrar");
		setBounds(100, 100, 215, 393);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);

		JLabel lblMatricula = new JLabel("Candidato");
		lblMatricula.setBounds(22, 111, 124, 15);
		getContentPane().add(lblMatricula);

		textFieldMatricula = new JTextField();
		textFieldMatricula.setText("matrícula");
		textFieldMatricula.setBounds(32, 138, 114, 19);
		getContentPane().add(textFieldMatricula);
		textFieldMatricula.setColumns(10);

		JLabel certificado = new JLabel("Certificado");
		certificado.setBounds(22, 169, 124, 15);
		getContentPane().add(certificado);

		textFieldAnoEvento = new JTextField();
		textFieldAnoEvento.setBounds(32, 196, 114, 19);
		getContentPane().add(textFieldAnoEvento);
		textFieldAnoEvento.setColumns(10);

		JLabel lblAtividade = new JLabel("Atividade");
		lblAtividade.setBounds(22, 12, 70, 15);
		getContentPane().add(lblAtividade);

		txtcompeticao = new JTextField();
		txtcompeticao.setText("atividade");
		txtcompeticao.setBounds(43, 31, 114, 19);
		getContentPane().add(txtcompeticao);
		txtcompeticao.setColumns(10);

		JLabel lblCredenciar = new JLabel("Credenciar");
		lblCredenciar.setBounds(22, 55, 124, 15);
		getContentPane().add(lblCredenciar);

		JRadioButton rdbtnSim = new JRadioButton("true");
		rdbtnSim.setBounds(22, 78, 70, 23);
		getContentPane().add(rdbtnSim);

		JRadioButton rdbtnNao = new JRadioButton("false");
		rdbtnNao.setBounds(100, 78, 70, 23);
		getContentPane().add(rdbtnNao);
		setVisible(true);

		JLabel lblData = new JLabel("Data");
		lblData.setBounds(22, 227, 70, 15);
		getContentPane().add(lblData);

		txtDdmmyyyy = new JTextField();
		txtDdmmyyyy.setText("dd/mm/aa");
		txtDdmmyyyy.setBounds(32, 254, 114, 19);
		getContentPane().add(txtDdmmyyyy);
		txtDdmmyyyy.setColumns(10);

		JButton btnCadastrarEvento = new JButton("Inscrição evento");
		btnCadastrarEvento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String competicao = txtcompeticao.getText();
				boolean certificado = Boolean.parseBoolean(rdbtnSim.getText());
				String anoEvento = textFieldAnoEvento.getText();
				String matricula = textFieldMatricula.getText();
				LocalDate horario;
				DateTimeFormatter formatado = DateTimeFormatter.ofPattern("dd/MM/yy");
				horario =  LocalDate.parse(txtDdmmyyyy.getText(), formatado);

				txtDdmmyyyy.getText();
				try {
					Atividade atividade = Fachada.buscarAtividadePeloNome(competicao);
					Candidato candidato = Fachada.buscarCandidatoPelaMatricula(matricula);
					Inscricao cadastro = Fachada.cadastrarInscricao(atividade, certificado, candidato, horario);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					
				} 
			}


		});
		btnCadastrarEvento.setBounds(22, 311, 163, 25);
		getContentPane().add(btnCadastrarEvento);
		setVisible(true);

	}
}

