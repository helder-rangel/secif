package aplicacaoswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Julgamento;

public class JulgamentoAtualiza extends JFrame {

	private JPanel contentPane;
	private JLabel lblQuesito;
	private JLabel lblQuesitoNovo;
	private JTextField txtQuesito;
	private JTextField txtQuesitoNovo;
	
	/**
	 * Create the application.
	 */
	public JulgamentoAtualiza() {
		initialize();
	}

	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Julgamento | Atualizar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblQuesito = new JLabel("Quesito");
		lblQuesito.setBounds(23, 12, 105, 15);
		getContentPane().add(lblQuesito);
		
		txtQuesito = new JTextField();
		txtQuesito.setText("Quesito");
		txtQuesito.setBounds(23, 39, 114, 19);
		getContentPane().add(txtQuesito);
		txtQuesito.setColumns(10);
		
		JLabel lblNovaMatrcula = new JLabel("Nova quesito");
		lblNovaMatrcula.setBounds(23, 70, 114, 15);
		getContentPane().add(lblNovaMatrcula);
		
		txtQuesitoNovo = new JTextField();
		txtQuesitoNovo.setText("Novo quesito");
		txtQuesitoNovo.setBounds(23, 99, 114, 19);
		getContentPane().add(txtQuesitoNovo);
		txtQuesitoNovo.setColumns(10);
		
		JButton btnAtualizarCandidato = new JButton("Atualizar quesito");
		btnAtualizarCandidato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String quesito = txtQuesito.getText();
				String novoQuesito = txtQuesitoNovo.getText();
				try {
					Julgamento atualCand = Fachada.atualizarQuesitoJulgamento(quesito,novoQuesito);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnAtualizarCandidato.setBounds(23, 130, 180, 25);
		getContentPane().add(btnAtualizarCandidato);
		setVisible(true);

		
	}

}
