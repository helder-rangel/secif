package modelo;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Inscricao")
public class Inscricao {
	
	@Id		
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	private Atividade competicao;
	
	private boolean certificado;
	
	@ManyToOne
	private Candidato candidato;
	
	private LocalDate dataInscricao;
	
	@OneToMany(mappedBy="inscricao", cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
	private ArrayList<Participacao> participacao;
	
	@ManyToOne
	private Evento evento;
	
	public Inscricao() {
		super();
		this.participacao = new ArrayList<Participacao>();
	}
	
	public Inscricao(Atividade competicao, boolean certificado, Candidato candidato, LocalDate dataInscricao) {
		super();
		this.competicao = competicao;
		this.certificado = certificado;
		this.candidato = candidato;
		this.dataInscricao = dataInscricao;
		this.participacao = new ArrayList<Participacao>();
	}
	
	public Atividade getCompeticao() {
		return competicao;
	}

	public boolean isCertificado() {
		return certificado;
	}

	public Candidato getCandidato() {
		return candidato;
	}

	public LocalDate getDataInscricao() {
		return dataInscricao;
	}

	public ArrayList<Participacao> getParticipacao() {
		return participacao;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setCompeticao(Atividade competicao) {
		this.competicao = competicao;
	}

	public void setCertificado(boolean certificado) {
		this.certificado = certificado;
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}

	public void setDataInscricao(LocalDate dataInscricao) {
		this.dataInscricao = dataInscricao;
	}

	public void setParticipacao(ArrayList<Participacao> participacao) {
		this.participacao = participacao;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	@Override
	public String toString() {
		return "Inscricao [competicao=" + competicao + ", certificado=" + certificado + ", candidato=" + candidato
				+ ", dataInscricao=" + dataInscricao + ", participacao=" + participacao + ", evento=" + evento + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((candidato == null) ? 0 : candidato.hashCode());
		result = prime * result + (certificado ? 1231 : 1237);
		result = prime * result + ((competicao == null) ? 0 : competicao.hashCode());
		result = prime * result + ((dataInscricao == null) ? 0 : dataInscricao.hashCode());
		result = prime * result + ((evento == null) ? 0 : evento.hashCode());
		result = prime * result + ((participacao == null) ? 0 : participacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inscricao other = (Inscricao) obj;
		if (candidato == null) {
			if (other.candidato != null)
				return false;
		} else if (!candidato.equals(other.candidato))
			return false;
		if (certificado != other.certificado)
			return false;
		if (competicao == null) {
			if (other.competicao != null)
				return false;
		} else if (!competicao.equals(other.competicao))
			return false;
		if (dataInscricao == null) {
			if (other.dataInscricao != null)
				return false;
		} else if (!dataInscricao.equals(other.dataInscricao))
			return false;
		if (evento == null) {
			if (other.evento != null)
				return false;
		} else if (!evento.equals(other.evento))
			return false;
		if (participacao == null) {
			if (other.participacao != null)
				return false;
		} else if (!participacao.equals(other.participacao))
			return false;
		return true;
	}

	public void adicionarParticipacao(Participacao participacao) throws Exception {
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacao)) {
				throw new Exception("Não e possivel inserir: participacao ja existente");
			}
		}
		
		this.participacao.add(participacao);		
	}
	
	public void alterarParticipacao(Participacao participacaoAntiga, Participacao participacaoNova) throws Exception{
		
		Participacao participacaoTemporaria = null;
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacaoAntiga)) {
				participacaoTemporaria = p;
				this.participacao.remove(p);
			}
		}
		
		if (participacaoTemporaria == null) {
			throw new Exception("Não e possivel alterar: participacao nao existente");
		}
		
		this.participacao.add(participacaoNova);		
	}
	
	public void removerParticipacao(Participacao participacao) throws Exception {
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacao)) {
				this.participacao.remove(participacao);
			}
		}
		
		throw new Exception("Não e possivel remover: participacao nao existente");
	}
	
	public String listarParticipacao() {
		
		String retorno = "Listagem das Participacoes\n";
		
		for (Participacao p: this.participacao) {
			retorno+= p + "\n";
		}
		
		return retorno;
	}
	
	public Participacao procurarParticipacao(Participacao participacao) throws Exception {
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacao)) {
				return p;
			}
		}
		
		throw new Exception("Nao e possivel coletar: participacao nao existente");
	}
}
