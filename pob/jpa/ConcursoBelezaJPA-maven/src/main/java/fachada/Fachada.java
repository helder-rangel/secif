package fachada;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import daojpa.DAO;
import daojpa.DAOAtividade;
import daojpa.DAOCandidato;
import daojpa.DAOEvento;
import daojpa.DAOInscricao;
import daojpa.DAOJuiz;
import daojpa.DAOJulgamento;
import daojpa.DAOParticipacao;
import daojpa.DAOPessoa;
import modelo.Atividade;
import modelo.Candidato;
import modelo.Evento;
import modelo.Inscricao;
import modelo.Juiz;
import modelo.Julgamento;
import modelo.Participacao;
import modelo.Pessoa;

public class Fachada {

	private static DAOPessoa daoPessoa = new DAOPessoa();
	private static DAOCandidato daoCandidato = new DAOCandidato();
	private static DAOJuiz daoJuiz = new DAOJuiz(); //TODO -> Modificado
	private static DAOAtividade daoAtividade = new DAOAtividade(); //TODO -> Modificado
	private static DAOInscricao daoInscricao = new DAOInscricao();
	private static DAOParticipacao daoParticipacao = new DAOParticipacao();
	private static DAOJulgamento daoJulgamento = new DAOJulgamento();
	private static DAOEvento daoEvento = new DAOEvento();

	public static void inicializar(){
		DAO.open();
	}

	public static void finalizar(){
		DAO.close();
	}


	//CADASTRAR OBJETOS | CREATE OBJECTS

	public static Pessoa cadastrarPessoa(String nome, String email, String cpf, String telefone) throws  Exception{

		DAO.begin();	
		Pessoa pessoa = daoPessoa.read(nome);

		if(pessoa != null) {
			DAO.rollback();
			throw new Exception("Erro, pessoa ja cadastrada: " + nome);
		}
			
		pessoa = new Pessoa(nome,email,cpf,telefone);
		daoPessoa.create(pessoa);	
		DAO.commit();		
		return pessoa;
	}	

	public static Candidato cadastrarCandidato(String nome, String email, String cpf, String telefone, String curso, String matricula) throws Exception{

		DAO.begin();
		Candidato candidato = daoCandidato.read(matricula);

		if(candidato != null) {
			DAO.rollback();
			throw new Exception("Erro, candidato ja cadastrado: " + nome);
		}

		candidato = new Candidato(nome,email,cpf,telefone,curso,matricula);
		daoCandidato.create(candidato);
		DAO.commit();
		return candidato;
	}

	public static Juiz cadastrarJuiz(String nome, String email, String cpf, String telefone, boolean externo, String instituicao) throws Exception{

		DAO.begin();
		Juiz juiz = daoJuiz.read(cpf);

		if(juiz != null) {
			DAO.rollback();
			throw new Exception("Erro, juiz ja cadastrado: " + nome);
		}
		
		juiz = new Juiz(nome,email,cpf,telefone,externo,instituicao);
		daoJuiz.create(juiz);
		DAO.commit();
		return juiz;
	}

	public static Evento cadastrarEvento(String nome, String ano) throws Exception {

		DAO.begin();
		Evento evento = daoEvento.read(nome);

		if(evento != null) {
			DAO.rollback();
			throw new Exception("Erro, evento ja cadastrado: " + nome);
		}

		evento = new Evento(nome, ano);
		daoEvento.create(evento);
		DAO.commit();
		return evento;
	}

	public static Atividade cadastrarAtividade(String nome, String descricao, double minimoParticipantes, double maximoParticipantes,
			LocalDate horario) throws Exception {
		/*TODO -> método de adicao de julgamentos para determinada atividade,
		 * 	caso exista no banco ele não será criado no momento.
		 */
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);

		if (atividade != null) {
			DAO.rollback();
			throw new Exception("Erro, atividade ja cadastrada: " + nome);
		}

		atividade = new Atividade(nome,descricao,minimoParticipantes,maximoParticipantes,horario);
		daoAtividade.create(atividade);
		DAO.commit();
		return atividade;
	}

	public static Inscricao cadastrarInscricao(Atividade competicao, boolean certificado, Candidato candidato, LocalDate dataInscricao) throws Exception{

		DAO.begin();
		Inscricao inscrito = daoInscricao.read(buscarCandidato(candidato) );

		if (inscrito != null) {
			DAO.rollback();
			throw new Exception("Erro, inscricao ja realizada: " + candidato);
		}

		inscrito = new Inscricao(competicao,certificado,candidato,dataInscricao);
		daoInscricao.create(inscrito);
		DAO.commit();
		return inscrito;
	}

	public static Participacao cadastrarParticipacao(boolean checkin, boolean certificado, double pontuacao) throws Exception{

		DAO.begin();
		Participacao participacao = daoParticipacao.read(checkin);

		if (participacao != null) {
			DAO.rollback();
			throw new Exception("Erro, participacao ja realizada: " + checkin);
		}

		participacao = new Participacao(checkin,certificado,pontuacao);
		daoParticipacao.create(participacao);
		DAO.commit();
		return participacao;
	}

	public static Julgamento cadastrarJulgamento(double nota, String quesito) throws Exception{

		DAO.begin();
		Julgamento julgamento = daoJulgamento.read(quesito);

		if(julgamento != null) {
			DAO.rollback();
			throw new Exception("Erro, julgamento ja cadastrado: " + quesito);		
		}

		julgamento = new Julgamento(nota,quesito);
		daoJulgamento.create(julgamento);
		DAO.commit();
		return julgamento;
	}

	//LISTAR TODOS OS OBJETOS | READ ALL OBJECTS

	public static String listarPessoas(){

		List<Pessoa> pessoas = daoPessoa.readAll();
		String texto="-----------Listagem de Pessoas-----------\n";

		for(Pessoa pessoa : pessoas){
			texto += pessoa + "\n";			
		}

		return texto;
	}

	public static String listarCandidatos(){

		List<Candidato> candidatos = daoCandidato.readAll();
		String texto="-----------Listagem de Candidatos---------\n";

		for (Candidato candidato : candidatos) {
			texto+= candidato + "\n";
		}

		return texto;
	}

	public static String listarJuizes(){

		List<Juiz> juizes = daoJuiz.readAll();
		String texto="-----------Listagem de Juizes--------------\n";

		for (Juiz juiz : juizes) {
			texto+= juiz + "\n";
		}

		return texto;
	}

	public static String listarEventos(){

		List<Evento> eventos = daoEvento.readAll();
		String texto="-----------Listagem de Eventos--------------\n";

		for (Evento evento : eventos) {
			texto+= evento + "\n";
		}

		return texto;
	}

	public static String listarAtividades(){

		List<Atividade> atividades = daoAtividade.readAll();
		String texto="-----------Listagem de Atividades-----------\n";

		for (Atividade atividade : atividades) {
			texto+= atividade + "\n";
		}

		return texto;
	}

	public static String listarInscricoes(){

		List<Inscricao> inscricoes = daoInscricao.readAll();
		String texto="-----------Listagem de Inscricoes------------\n";

		for (Inscricao in : inscricoes) {
			texto+= in + "\n";
		}

		return texto;
	}

	public static String listarParticipacoes(){

		List<Participacao> participacoes = daoParticipacao.readAll();
		String texto="-----------Listagem de Participacoes----------\n";

		for (Participacao participacao : participacoes) {
			texto+= participacao + "\n";
		}

		return texto;
	}

	public static String listarJulgamentos(){

		List<Julgamento> julgamentos = daoJulgamento.readAll();
		String texto="-----------Listagem de Participacoes----------\n";

		for (Julgamento julgamento : julgamentos) {
			texto+= julgamento + "\n";
		}

		return texto;
	}


	//CONSULTAR OBJETOS | READ OBJECTS

	public static Atividade buscarAtividade(Atividade atividade) throws Exception {

		DAO.begin();
		Atividade atividadeTemporaria = daoAtividade.read(atividade.getNome());

		if (atividadeTemporaria == null) {
			DAO.rollback();
			throw new Exception("Candidato inexistente.");
		}

		return atividade;
	}
	
	public static Atividade buscarAtividadePeloNome(String nome) throws Exception {

		DAO.begin();
		Atividade atividadeTemporaria = daoAtividade.read(nome);

		if (atividadeTemporaria == null) {
			DAO.rollback();
			throw new Exception("Atividade inesistente.");
		}

		return atividadeTemporaria;
	}
	
	public static String buscarAtividadePeloDia(String diaAtividade) throws Exception {
		//TODO
		LocalDate horario;
		DateTimeFormatter formatado = DateTimeFormatter.ofPattern("dd/MM/yy");
		horario =  LocalDate.parse(diaAtividade, formatado);
		List<Atividade> atividadeTemporaria = daoAtividade.buscarAtividadesPeloDia(horario);
		
		if (atividadeTemporaria == null) {
			DAO.rollback();
			throw new Exception("Atividade inesistente neste dia.");
		}

		String resultado = "Lista de Atividades no dia " + diaAtividade;
		for (Atividade atividade : atividadeTemporaria) {
			resultado += "\n\n" + "Nome: " + atividade.getNome() + 
					"\nDescricao" + atividade.getDescricao() + 
					"\nEvento" + atividade.getEvento().getNome();
		}
		return resultado;
	}

	public static Atividade buscarAtividadePeloNomeEEvento(String nomeAtividade, String nomeEvento) throws Exception {
	//TODO
	Atividade atividadeTemporaria = daoAtividade.buscarPeloNomeEEvento(nomeAtividade, nomeEvento);
	
	if (atividadeTemporaria == null) {
		DAO.rollback();
		throw new Exception("Atividade inesistente.");
	}

	return atividadeTemporaria;
	}
	
	public static String buscarAtividadeParticipantes(String nomeAtividade, String nomeEvento) throws Exception {
		//TODO
		List<String> atividadeTemporaria = daoAtividade.buscarParticipantes(nomeAtividade, nomeEvento);
		
		if (atividadeTemporaria == null)
			throw new Exception("Atividade inesistente.");

		String resultado = "Lista de Participantes na Atividade " + nomeAtividade + "\n";
		
		for (String candidatos : atividadeTemporaria) {
			resultado += "\n" + "Nome Candidatos: " + candidatos;					
		}
		
		return resultado;
	}
	
	public static String buscarAtividadeJulgadores(String nomeAtividade, String nomeEvento) throws Exception {
		//TODO
		List<String> atividadeTemporaria = daoAtividade.buscarJulgadores(nomeAtividade, nomeEvento);
		
		if (atividadeTemporaria == null)
			throw new Exception("Atividade inesistente.");

		String resultado = "Lista de Julgadores na Atividade " + nomeAtividade + "\n";
		
		for (String candidatos : atividadeTemporaria) {
			resultado += "\n" + "Nome Julgadores: " + candidatos;					
		}
		
		return resultado;
	}
	
	public static Candidato buscarCandidato(Candidato candidato) throws Exception {

		DAO.begin();
		Candidato candidatoTemporario = daoCandidato.read(candidato.getMatricula());

		if (candidatoTemporario == null)
			throw new Exception("Candidato inesistente.");

		return candidato;
	}
	
	public static Candidato buscarCandidatoPelaMatricula(String matricula) throws Exception {
	//TODO
		DAO.begin();
		Candidato candidatoTemporario = daoCandidato.read(matricula);

		if (candidatoTemporario == null)
			throw new Exception("Candidato inesistente.");

		return candidatoTemporario;
	}
	
	public static String buscarCandidatoPorCurso (String curso){
		
		List<Candidato> resultado = daoCandidato.buscarPorCurso(curso);
		
		String saida = "\n Lista de candidatos do curso: "+curso;
		if (resultado.isEmpty())  
			saida += "\n Consulta vazia";
		else 
			for(Pessoa p: resultado)saida += "\n" + p;
		return saida;
	}
	
	public static String buscarCandidatoAtividades(String matricula, String dataInscricao) throws Exception {
		//TODO
		LocalDate horario;
		DateTimeFormatter formatado = DateTimeFormatter.ofPattern("dd/MM/yy");
		horario =  LocalDate.parse(dataInscricao, formatado);
		List<String> atividades = buscarInscricaoAtividades(matricula,dataInscricao);
		
		if (atividades == null)
			throw new Exception("Atividades cadastradas inesistentes.");

		String resultado = "Lista de Atividades do Candidato \n";
		
		for (String candidatos : atividades) {
			resultado += "\n" + "Nome Atividades: " + candidatos;					
		}
		
		return resultado;
		
	}
	
	public static Evento buscarEventoPeloNome(String nomeEvento) throws Exception {
		//TODO
		DAO.begin();
		Evento eventoTemporario = daoEvento.read(nomeEvento);
		
		if(eventoTemporario == null) {
			throw new Exception("Evento Inesistente");
		}
		
		DAO.commit();
		return eventoTemporario;
	}
	
	public static Inscricao buscarInscricaoPorMatricula(String matricula) throws Exception {
		//TODO
		Candidato candidatoTemporario = buscarCandidatoPelaMatricula(matricula);
		DAO.begin();
		Inscricao inscricao = daoInscricao.read(candidatoTemporario);
		
		if (inscricao == null) {
			throw new Exception("Inscricao Inesistente");
		}
		
		return inscricao;
	}
	
	//TODO -> metodo que busca a inscricao pela metricula e data aqui
	public static Inscricao buscarInscricaoPelaMatriculaEData (String matricula, String data) throws Exception{
		Candidato candidatoTemporario = buscarCandidatoPelaMatricula(matricula);
		LocalDate horario;
		DateTimeFormatter formatado = DateTimeFormatter.ofPattern("dd/MM/yy");
		horario =  LocalDate.parse(data, formatado);
		DAO.begin();
		Inscricao inscricao = daoInscricao.buscarPorMatriculaEData(candidatoTemporario, horario);
		
		if (inscricao == null) {
			throw new Exception("Inscricao Inesistente");
		}
		
		return inscricao;
	}
	
	public static List<String> buscarInscricaoAtividades (String matricula, String data) throws Exception{
		Candidato candidatoTemporario = buscarCandidatoPelaMatricula(matricula);
		LocalDate horario;
		DateTimeFormatter formatado = DateTimeFormatter.ofPattern("dd/MM/yy");
		horario =  LocalDate.parse(data, formatado);
		DAO.begin();
		List<String> inscricoes = daoInscricao.buscarAtividades(candidatoTemporario, horario);
		
		if (inscricoes == null) {
			throw new Exception("Inscricao Inesistente");
		}
		
		return inscricoes;
	}
	
	
	public static Juiz buscarJuizPeloCpf(String cpf) throws Exception {
		//TODO
		DAO.begin();
		Juiz juiz = daoJuiz.read(cpf);
		
		if (juiz == null) {
			throw new Exception("Juiz Inesistente");
		}
		
		return juiz;
	}
	
	public static Juiz buscarJuizPeloNome(String nome) throws Exception {
		//TODO
		DAO.begin();
		Juiz juiz = daoJuiz.buscarPeloNome(nome);
		
		if (juiz == null) {
			throw new Exception("Juiz Inesistente");
		}
		
		return juiz;
	}

	//ATUALIZAR OBJETOS | UPDATE OBJECTS

 	public static Pessoa atualizarNomePessoa(String nome, String novo) throws Exception {

		DAO.begin();
		Pessoa pessoa = daoPessoa.read(nome);

		if(pessoa == null) throw new Exception ("Pessoa inexistente, não é possível atulizá-la.");

		pessoa.setNome(novo);
		daoPessoa.update(pessoa);
		DAO.commit();
		return pessoa;
	}

	public static Pessoa atualizarEmailPessoa(String nome, String email, String novo) throws Exception {

		DAO.begin();
		Pessoa pessoa = daoPessoa.read(nome);

		if(!pessoa.getEmail().equals(email)) throw new Exception ("E-mail inexistente, não é possível atulizá-lo.");


		pessoa.setEmail(novo);
		daoPessoa.update(pessoa);
		DAO.commit();
		return pessoa;
	}

	public static Pessoa atualizarTelefonePessoa(String nome, String telefone, String novo) throws Exception {

		DAO.begin();
		Pessoa pessoa = daoPessoa.read(nome);

		if(!pessoa.getTelefone().equals(telefone) ) throw new Exception ("Telefone inexistente, não é possível atulizá-lo.");


		pessoa.setTelefone(novo);
		daoPessoa.update(pessoa);
		DAO.commit();
		return pessoa;
	}

	public static Pessoa atualizarCpfPessoa(String nome, String cpf, String novo) throws Exception {

		DAO.begin();
		Pessoa pessoa = daoPessoa.read(cpf);

		if(!pessoa.getCpf().equals(cpf) ) throw new Exception ("Cpf inexistente, não é possível atulizá-lo.");


		pessoa.setCpf(novo);
		daoPessoa.update(pessoa);
		DAO.commit();
		return pessoa;
	}

	public static Candidato atualizarMatriculaCandidato(String matricula, String nova) throws Exception {
		
		DAO.begin();
		Candidato candidato = daoCandidato.read(matricula);

		if(candidato == null) {
			throw new Exception ("Matricula inexistente, não é possível atulizá-la.");
		}


		candidato.setMatricula(nova);
		daoPessoa.update(candidato);
		DAO.commit();
		return candidato;
	}
	
	public static Candidato atualizarCursoCandidato(String matricula, String curso, String nova) throws Exception {
		
		DAO.begin();
		Candidato candidato = daoCandidato.read(matricula);
		
		
		if(!candidato.getCurso().equals(curso)) {
			throw new Exception ("Curso inexistente, nao eh possivel atuliza-lo.");
		} else if(candidato.getCurso().equals(curso)){
			throw new Exception ("Curso ja eh o do candidato, nao eh possivel atuliza-lo.");
		}


		candidato.setCurso(nova);
		daoPessoa.update(candidato);
		DAO.commit();
		return candidato;
	}
	
	public static Inscricao adicionarInscricaoCandidato(String matricula) throws Exception {
		//TODO ->
		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Candidato candidato = daoCandidato.read(matricula);
		if(candidato == null)
			throw new Exception("Adicionar Candidato Inscricao- candidato nao cadastrado:" );

		
		if(candidato.procurarInscricao(inscricao) != null)
			throw new Exception("adicionar inscricao ao candidato - inscricao ja cadastrado:" );

		
		candidato.adicionarInscricao(inscricao);
		daoCandidato.update(candidato);	
		DAO.commit();
		return inscricao;
	}
	
	public static Evento adicionarInscricaoEvento(String matricula) throws Exception {
		//TODO ->
		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Evento evento = daoEvento.read(matricula);
		if(evento == null)
			throw new Exception("Adicionar Evento Inscricao- evento nao cadastrado:");

		
		if(evento.procurarInscricao(inscricao) != null)
			throw new Exception("adicionar inscricao ao Evento - inscricao ja cadastrado:" );

		
		evento.adicionarInscricao(inscricao);
		daoEvento.update(evento);
		DAO.commit();
		return evento;
	}
	
	public static Atividade adicionarJulgamentoAtividade(String nomeAtividade, Julgamento julgamento) throws Exception {
		//TODO ->
//		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Atividade atividade = daoAtividade.read(nomeAtividade);
		if(atividade == null)
			throw new Exception("Atividade nao cadastrada:" );

		
		if(atividade.procurarJulgamento(julgamento) != null)
			throw new Exception("adicionar julgamento a atividade- julgamento ja cadastrado:");

		
		atividade.adicionarJulgamento(julgamento);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade adicionarParticipacaoAtividade(String nomeAtividade, Participacao participacao) throws Exception {
		//TODO ->
		DAO.begin();	
		Atividade atividade = daoAtividade.read(nomeAtividade);
		if(atividade == null)
			throw new Exception("Atividade nao cadastrada:" );

		
		if(atividade.procurarParticipacao(participacao) != null)
			throw new Exception("adicionar participacao a atividade- participacao ja cadastrado:");

		
		atividade.adicionarParticipacao(participacao);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Juiz adicionarJulgamentoJuiz(String cpf, Julgamento julgamento) throws Exception {
		//TODO ->
//		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Juiz juiz= daoJuiz.read(cpf);
		if(juiz == null)
			throw new Exception("juiz nao cadastrado(a):");

		
		if(juiz.procurarJulgamento(julgamento) != null)
			throw new Exception("adicionar julgamento ao juiz- julgamento ja cadastrado:");

		
		juiz.adicionarJulgamento(julgamento);
		daoJuiz.update(juiz);
		DAO.commit();
		return juiz;
	}
	
	public static Inscricao adicionarParticipacaoInscricao(String matricula, Participacao participacao) throws Exception {
		//TODO ->
		DAO.begin();	
		Inscricao inscricao = buscarInscricaoPorMatricula(matricula);
		
		if(inscricao== null)
			throw new Exception("inscricao nao cadastrado(a):");

		
		if(inscricao.procurarParticipacao(participacao) != null)
			throw new Exception("adicionar julgamento ao juiz- julgamento ja cadastrado:");

		
		inscricao.adicionarParticipacao(participacao);
		daoInscricao.update(inscricao);
		DAO.commit();
		return inscricao;
	}
	
	public static Inscricao removerInscricaoCandidato(String matricula) throws Exception {
		//TODO ->
		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Candidato candidato = daoCandidato.read(matricula);
		if(candidato == null)
			throw new Exception("Adicionar Candidato Inscricao- candidato nao cadastrado:");

		
		if(candidato.procurarInscricao(inscricao) == null)
			throw new Exception("remover inscricao ao candidato - inscricao nao cadastrado:" );

		
		candidato.removerInscricao(inscricao);
		daoCandidato.update(candidato);	
		DAO.commit();
		return inscricao;
	}
	
	public static Evento removerInscricaoEvento(String matricula) throws Exception {
		//TODO ->
		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Evento evento = daoEvento.read(matricula);
		if(evento == null)
			throw new Exception("Adicionar Evento Inscricao- evento nao cadastrado:" );

		
		if(evento.procurarInscricao(inscricao) == null)
			throw new Exception("remover inscricao ao Evento - inscricao nao cadastrado:");

		
		evento.removerInscricao(inscricao);
		daoEvento.update(evento);
		DAO.commit();
		return evento;
	}
	
	public static Atividade removerJulgamentoAtividade(String nomeAtividade, Julgamento julgamento) throws Exception {
		//TODO ->
//		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Atividade atividade = daoAtividade.read(nomeAtividade);
		if(atividade == null)
			throw new Exception("Atividade nao cadastrada:" + atividade.getNome());

		
		if(atividade.procurarJulgamento(julgamento) == null)
			throw new Exception("remover julgamento a atividade- julgamento nao cadastrado:");

		
		atividade.removerJulgamento(julgamento);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade removerParticipacaoAtividade(String nomeAtividade, Participacao participacao) throws Exception {
		//TODO ->
		DAO.begin();	
		Atividade atividade = daoAtividade.read(nomeAtividade);
		if(atividade == null)
			throw new Exception("Atividade nao cadastrada:" + atividade.getNome());

		
		if(atividade.procurarParticipacao(participacao) == null)
			throw new Exception("remover participacao a atividade- participacao nao cadastrado:");

		
		atividade.removerParticipacao(participacao);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Juiz removerJulgamentoJuiz(String cpf, Julgamento julgamento) throws Exception {
		//TODO ->
//		Inscricao inscricao  = buscarInscricaoPorMatricula(matricula);
		DAO.begin();	
		Juiz juiz= daoJuiz.read(cpf);
		if(juiz == null)
			throw new Exception("juiz nao cadastrado(a):");

		
		if(juiz.procurarJulgamento(julgamento) == null)
			throw new Exception("remover julgamento ao juiz- julgamento nao cadastrado:");

		
		juiz.removerJulgamento(julgamento);
		daoJuiz.update(juiz);
		DAO.commit();
		return juiz;
	}
	
	public static Inscricao removerParticipacaoInscricao(String matricula, Participacao participacao) throws Exception {
		//TODO ->
		DAO.begin();	
		Inscricao inscricao = buscarInscricaoPorMatricula(matricula);
		
		if(inscricao== null)
			throw new Exception("inscricao nao cadastrado(a):");

		
		if(inscricao.procurarParticipacao(participacao) == null)
			throw new Exception("remover julgamento ao juiz- julgamento nao cadastrado:");

		
		inscricao.removerParticipacao(participacao);
		daoInscricao.update(inscricao);
		DAO.commit();
		return inscricao;
	}
	
	public static Juiz atualizarInstituicaoJuiz(String cpf, String instituicao, String nova) throws Exception{
		
		DAO.begin();
		Juiz juiz = daoJuiz.read(cpf);
		
		if(juiz.getInstituicao().equals(instituicao)) {
			throw new Exception ("Juiz ja eh o da instituicao, nao eh possivel atuliza-lo.");
		}
		
		juiz.setInstituicao(nova);
		daoJuiz.update(juiz);
		DAO.commit();
		return juiz;
	}
	
	public static Juiz atualizarExternoJuiz(String cpf, boolean externo, boolean novo) throws Exception{
		
		DAO.begin();
		Juiz juiz = daoJuiz.read(cpf);
		
		if (juiz == null) {
			throw new Exception ("Juiz nao encontrado(a) com o cpf: " + cpf );
		} else if(juiz.isExterno() == externo) {
			throw new Exception ("Juiz ja eh o de tal lugar, nao eh possivel atuliza-lo.");
		}
		
		juiz.setExterno(novo);
		daoJuiz.update(juiz);
		DAO.commit();
		return juiz;
	}
	
	public static Atividade atualizarNomeAtividade (String nome, String novo) throws Exception {
		
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);
		
		if(atividade == null) {
			throw new Exception ("Atividade nao encontrada com o nome: " + nome );
		} else if(atividade.getNome().equals(nome)) {
			throw new Exception ("Nome de atividade ja existente, nao e possivel alterar");
		}
		
		atividade.setNome(novo);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade atualizarDescricaoAtividade (String nome ,String novaDescricao) throws Exception {
		
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);
		
		if(atividade == null) {
			throw new Exception ("Atividade nao encontrada com o nome: " + nome );
		} else if(atividade.getDescricao().equals(novaDescricao)) {
			throw new Exception ("Descricao de atividade ja existente, nao e possivel alterar");
		}
		
		atividade.setDescricao(novaDescricao);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade atualizarNumMaxAtividade (String nome ,double qtdMaxNova) throws Exception {
		
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);
		
		if(atividade == null) {
			throw new Exception ("Atividade nao encontrada com o nome: " + nome );
		} else if(atividade.getMaximoParticipantes() == qtdMaxNova) {
			throw new Exception ("Numero maximo de participantes da atividade ja existente, nao e possivel alterar");
		}
		
		atividade.setMaximoParticipantes(qtdMaxNova);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade atualizarNumMinAtividade (String nome ,double qtdMinNova) throws Exception {
		
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);
		
		if(atividade == null) {
			throw new Exception ("Atividade nao encontrada com o nome: " + nome );
		} else if(atividade.getMinimoParticipantes() == qtdMinNova) {
			throw new Exception ("Numero minimo de participantes da atividade ja existente, nao e possivel alterar");
		}
		
		atividade.setMinimoParticipantes(qtdMinNova);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade atualizarHorarioAtividade (String nome ,LocalDate horario) throws Exception {
		
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);
		
		if(atividade == null) {
			throw new Exception ("Atividade nao encontrada com o nome: " + nome );
		} else if(atividade.getHorario().equals(horario)) {
			throw new Exception ("Horario da atividade ja existente, nao e possivel alterar");
		}
		
		atividade.setHorario(horario);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade atualizarListaJulgamentosAtividade (String nome , ArrayList<Julgamento> pontuacao) throws Exception {
		
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);
		
		if(atividade == null) {
			throw new Exception ("Atividade nao encontrada com o nome: " + nome );
		} else if(atividade.getPontuacao().equals(pontuacao)) {
			throw new Exception ("Lista de julgamentos da atividade ja existente, nao e possivel alterar");
		}
		
		atividade.setPontuacao(pontuacao);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Atividade atualizarEventoAtividade( String nomeAtividade, String nomeEvento) throws Exception {
		//TODO
		Evento eventoTemporario = buscarEventoPeloNome(nomeEvento);
		DAO.begin();
		Atividade atividade = daoAtividade.read(nomeAtividade);
		
		if(atividade == null) {
			throw new Exception ("Atividade nao encontrada com o nome: " + nomeAtividade );
		} else if(atividade.getEvento().equals(eventoTemporario)) {
			throw new Exception ("Evento já cadastrado nesta atividade");
		}
		
		atividade.setEvento(eventoTemporario);
		daoAtividade.update(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Evento atualizarNomeEvento (String nome, String novoNome) throws Exception {
		
		DAO.begin();
		Evento evento = daoEvento.read(nome);
		
		if(evento == null) {
			throw new Exception ("Evento nao encontrado com o nome: " + nome );
		} else if(evento.getNome().equals(novoNome) ) {
			throw new Exception ("Nome do evento ja existente, nao e possivel alterar");
		}
		
		evento.setNome(novoNome);
		daoEvento.update(evento);
		DAO.commit();
		return evento;
	}
	
	public static Evento atualizarAnoEvento (String nome, String ano, String novoAno) throws Exception {
		
		DAO.begin();
		Evento evento = daoEvento.read(nome);
		
		if(evento == null) {
			throw new Exception ("Evento nao encontrado com o nome: " + nome );
		} else if(evento.getAno().equals(ano) || evento.getAno().equals(novoAno) ) {
			throw new Exception ("Ano do evento ja existente, nao e possivel alterar");
		}
		
		evento.setAno(novoAno);
		daoEvento.update(evento);
		DAO.commit();
		return evento;
	}
	
	public static Inscricao atualizarEventoInscricao( String matricula, String nomeEvento) throws Exception {
		//TODO
		Evento eventoTemporario = buscarEventoPeloNome(nomeEvento);
		DAO.begin();
		Inscricao inscricao = buscarInscricaoPorMatricula(matricula);
		
		if(inscricao == null) {
			throw new Exception ("Inscricao nao encontrada com o(a) candidato(a) de nome: " + inscricao.getCandidato().getNome() );
		} else if(inscricao.getEvento().equals(eventoTemporario)) {
			throw new Exception ("Evento já cadastrado nesta inscricao");
		}
		
		inscricao.setEvento(eventoTemporario);
		daoInscricao.update(inscricao);
		DAO.commit();
		return inscricao;
	}
	
	public static Inscricao atualizarCompeticaoInscricao (Candidato candidato, Atividade atividade, Atividade novaAtividade) throws Exception {
		
		DAO.begin();
		Inscricao inscricao = daoInscricao.read(candidato);
		
		if(inscricao == null) {
			throw new Exception ("Inscricao na atividade nao encontrada com o nome: " + atividade.getNome() );
		} else if(inscricao.getCompeticao().equals(novaAtividade) ) {
			throw new Exception ("Inscricao na atividade ja registrada, nao e possivel alterar");
		}
		
		inscricao.setCompeticao(novaAtividade);
		daoInscricao.update(inscricao);
		DAO.commit();
		return inscricao;
	}
	
	public static Inscricao atualizarCertificadoInscricao (Candidato candidato, boolean certificado, boolean novoCertificado) throws Exception {
		
		DAO.begin();
		Inscricao inscricao = daoInscricao.read(candidato);
		
		if(inscricao == null) {
			throw new Exception ("Inscricao nao encontrada com o nome: " + candidato.getNome() );
		} else if(inscricao.isCertificado() == novoCertificado) {
			throw new Exception ("Estado do certificado da inscricao ja registrada, nao e possivel alterar");
		}
		
		inscricao.setCertificado(novoCertificado);
		daoInscricao.update(inscricao);
		DAO.commit();
		return inscricao;
	}
	
	public static Inscricao atualizarDataInscricao (Candidato candidato, LocalDate novaDataInscricao) throws Exception {
		
		DAO.begin();
		Inscricao inscricao = daoInscricao.read(candidato);
		
		if(inscricao == null) {
			throw new Exception ("Inscricao nao encontrada com o nome: " + candidato.getNome() );
		} else if(inscricao.getDataInscricao().equals(novaDataInscricao)) {
			throw new Exception ("Data da inscricao ja registrada, nao e possivel alterar");
		}
		
		inscricao.setDataInscricao(novaDataInscricao);
		daoInscricao.update(inscricao);
		DAO.commit();
		return inscricao;
	}
	
	public static Julgamento atualizarQuesitoJulgamento(String quesito, String novoQuesito) throws Exception {
		
		DAO.begin();
		Julgamento julgamento = daoJulgamento.read(quesito);
		
		if(julgamento == null) {
			throw new Exception ("Julgamento nao encontrada com o quesito: " + julgamento.getQuesito() );
		} else if(julgamento.getQuesito().equals(novoQuesito)) {
			throw new Exception ("Quesito do julgamento ja registrada, nao e possivel alterar");
		}
		
		julgamento.setQuesito(novoQuesito);
		daoJulgamento.update(julgamento);
		DAO.commit();
		return julgamento;
	}
	
	public static Julgamento atualizarNotaJulgamento(String quesito, double novaNota) throws Exception {
		//TODO
		DAO.begin();
		Julgamento julgamento = daoJulgamento.read(quesito);
		
		if(julgamento == null) {
			throw new Exception ("Julgamento nao encontrada com o quesito: " + quesito );
		} else if(julgamento.getNota() == novaNota) {
			throw new Exception ("A nota do quesito no julgamento ja registrada, nao e possivel alterar");
		}
		
		julgamento.setNota(novaNota);
		daoJulgamento.update(julgamento);
		DAO.commit();
		return julgamento;
	}
	
	
	
	public static Julgamento atualizarAtividadeJulgamento(String quesito, String nomeAtividade ) throws Exception {
		//TODO
		Atividade atividadeTemporaria = buscarAtividadePeloNome(nomeAtividade);
		DAO.begin();
		Julgamento julgamento = daoJulgamento.read(quesito);
		
		if(julgamento == null) {
			throw new Exception ("Julgamento nao encontrado com o nome: " + quesito );
		} else if(julgamento.getAtividade().equals(atividadeTemporaria)) {
			throw new Exception ("Juiz já cadastrado neste julgamento");
		}
		julgamento.setAtividade(atividadeTemporaria);
		daoJulgamento.update(julgamento);
		DAO.commit();
		return julgamento;
	}
	
	public static Julgamento atualizarJuizJulgamento(String quesito, String cpf ) throws Exception {
		//TODO
		Juiz juizTemporario =  buscarJuizPeloCpf(cpf);
		DAO.begin();
		Julgamento julgamento = daoJulgamento.read(quesito);
		
		if(julgamento == null) {
			throw new Exception ("Julgamento nao encontrado com o nome: " + quesito );
		} else if(julgamento.getJuiz().equals(juizTemporario)) {
			throw new Exception ("Juiz já cadastrado neste julgamento");
		}
		julgamento.setJuiz(juizTemporario);
		daoJulgamento.update(julgamento);
		DAO.commit();
		return julgamento;
	}
	
	public static Participacao atualizarCheckinParticipacao (boolean checkin, boolean novoCheckin) throws Exception{
		DAO.begin();
		Participacao participacao = daoParticipacao.read(checkin);
		
		if(participacao == null) {
			throw new Exception ("Participacao nao encontrada" );
		} else if(participacao.isCheckin() == novoCheckin) {
			throw new Exception ("O estado do checkin da participacao ja esta registrada, nao e possivel alterar");
		}
		
		participacao.setCheckin(novoCheckin);
		daoParticipacao.update(participacao);
		DAO.commit();
		return participacao;
	}
	
	public static Participacao atualizarCertificadoParticipacao (boolean checkin, boolean novoCertificado) throws Exception{
		DAO.begin();
		Participacao participacao = daoParticipacao.read(checkin);
		
		if(participacao == null) {
			throw new Exception ("Participacao nao encontrada" );
		} else if(participacao.isCertificado() == novoCertificado) {
			throw new Exception ("O estado do certificado da participacao ja esta registrada, nao e possivel alterar");
		}
		
		participacao.setCertificado(novoCertificado);
		daoParticipacao.update(participacao);
		DAO.commit();
		return participacao;
	}
	
	public static Participacao atualizarPontuacaoParticipacao (boolean checkin, double novaPontuacao) throws Exception{
		DAO.begin();
		Participacao participacao = daoParticipacao.read(checkin);
		
		if(participacao == null) {
			throw new Exception ("Participacao nao encontrada" );
		} else if(participacao.getPontuacao() == novaPontuacao) {
			throw new Exception ("O valor da potuacao da participacao ja esta registrada, nao e possivel alterar");
		}
		
		participacao.setPontuacao(novaPontuacao);
		daoParticipacao.update(participacao);
		DAO.commit();
		return participacao;
	}
	
	public static Participacao atualizarAtividadeParticipacao(boolean checkin, String nomeAtividade) throws Exception {
		//TODO
		Atividade atividade = buscarAtividadePeloNome(nomeAtividade);
		DAO.begin();
		Participacao participacao = daoParticipacao.read(checkin);
		
		if(participacao == null) {
			throw new Exception ("Participacao nao encontrada" );
		} else if(participacao.getAtividade().equals(atividade)) {
			throw new Exception ("A atividade da participacao ja esta registrada, nao e possivel alterar");
		}
		participacao.setAtividade(atividade);
		daoParticipacao.update(participacao);
		DAO.commit();
		return participacao;
	}
	
	public static Participacao atualizarInscricaoParticipacao(boolean checkin, String matricula) throws Exception {
		//TODO
		Inscricao inscricao = buscarInscricaoPorMatricula(matricula);
		DAO.begin();
		Participacao participacao = daoParticipacao.read(checkin);
		
		if(participacao == null) {
			throw new Exception ("Participacao nao encontrada" );
		} else if(participacao.getInscricao().equals(inscricao)) {
			throw new Exception ("A inscricao da participacao ja esta registrada, nao e possivel alterar");
		}
		participacao.setInscricao(inscricao);
		daoParticipacao.update(participacao);
		DAO.commit();
		return participacao;
	}
	
	//EXCLUIR OBJETOS | DELETE OBJECTS

	public static Atividade apagarAtividade(String nome) throws Exception {
		DAO.begin();
		Atividade atividade = daoAtividade.read(nome);
		
		if (atividade == null) {
			DAO.rollback();
			throw new Exception ("Atividade inexistente, não é possível apaga-la.");
		}
		
		daoAtividade.delete(atividade);
		DAO.commit();
		return atividade;
	}
	
	public static Candidato apagarCandidato(String matricula) throws Exception {
		
		DAO.begin();
		Candidato candidato = daoCandidato.read(matricula);
		
		if(candidato == null) {
			DAO.rollback();
			throw new Exception ("Candidato inexistente, não é possível apaga-lo(a).");
		}
		
	
		daoCandidato.delete(candidato);
		DAO.commit();
		return candidato;
	}
		
	public static Evento apagarEvento(String nome) throws Exception {
		
		DAO.begin();
		Evento evento = daoEvento.read(nome);
		
		if (evento == null) {
			DAO.rollback();
			throw new Exception ("Evento inexistente, não é possível apaga-lo.");
		}
		
		daoEvento.delete(evento);
		DAO.commit();
		return evento;
	}
	
	public static Inscricao apagarInscricao(Candidato candidato) throws Exception {

		DAO.begin();
		Inscricao inscricao = daoInscricao.read(candidato);
		
		if (inscricao == null) {
			DAO.rollback();
			throw new Exception ("Inscricao inexistente, não é possível apaga-la.");
		}
		
		daoInscricao.delete(inscricao);
		DAO.commit();
		return inscricao;
	}
	
	public static Juiz apagarJuiz(String cpf) throws Exception {
		
		DAO.begin();
		Juiz juiz = daoJuiz.read(cpf);
		
		if(juiz == null) {
			DAO.rollback();
			throw new Exception ("Pessoa inexistente, não é possível apaga-lo(a).");
		}
		
		daoPessoa.delete(juiz);
		DAO.commit();
		return juiz;
	}
	
	public static Julgamento apagarJulgamento(String quesito) throws Exception {

		DAO.begin();
		Julgamento julgamento = daoJulgamento.read(quesito);
		
		if (julgamento == null) {
			DAO.rollback();
			throw new Exception ("Julgamento inexistente, não é possível apaga-lo(a).");
		}
		
		daoJulgamento.delete(julgamento);
		DAO.commit();
		return julgamento;
	}
	
	public static Participacao apagarParticipacao(boolean checkin) throws Exception {

		DAO.begin();
		Participacao participacao = daoParticipacao.read(checkin);
		
		if (participacao == null) {
			DAO.rollback();
			throw new Exception ("Participacao inexistente, não é possível apaga-la.");
		}
		
		daoParticipacao.delete(participacao);
		DAO.commit();
		return participacao;
	}
	
	public static Pessoa apagarPessoa(String nome) throws Exception {
		
		DAO.begin();
		Pessoa pessoa = daoPessoa.read(nome);
		
		if(pessoa == null) {
			DAO.rollback();
			throw new Exception ("Pessoa inexistente, não é possível apaga-la.");
		}
		
	
		daoPessoa.delete(pessoa);
		DAO.commit();
		return pessoa;
	}

}
