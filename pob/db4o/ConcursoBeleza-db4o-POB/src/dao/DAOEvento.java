package dao;

import java.util.List;

import com.db4o.query.Query;

import modelo.Evento;

public class DAOEvento extends DAO<Evento>{

	public Evento read (Object chave) {
		String nome = (String) chave;
		Query q = manager.query();
		q.constrain(Evento.class);
		q.descend("nome").constrain(nome);
		List<Evento> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
}

