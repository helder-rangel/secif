package dao;

import java.time.LocalDate;
import java.util.List;

import com.db4o.query.Query;

import modelo.Candidato;
import modelo.Inscricao;

public class DAOInscricao extends DAO<Inscricao> {

	public Inscricao read (Object chave) {
		Candidato candidato = (Candidato) chave;
		Query q = manager.query();
		q.constrain(Inscricao.class);
		q.descend("candidato").constrain(candidato);
		List<Inscricao> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}

	//TODO -> metodo que busca e retorna por matricula e data
	public Inscricao buscarPorMatriculaEData(Candidato candidato, LocalDate data) {
		Query q = manager.query();
		q.constrain(Inscricao.class);
		q.descend("candidato").constrain(candidato).and(
				q.descend("dataInscricao").constrain(data) );
		List<Inscricao> resultado = q.execute();
		if (resultado.size()>0)
			return resultado.get(0);
		else
			return null;
	}

	public List<String> buscarAtividades(Candidato candidato, LocalDate data) {
		Inscricao inscricaoTmp = buscarPorMatriculaEData(candidato,data);
		Query q = manager.query();
		q.constrain(Inscricao.class);
		//		q.descend("candidato").constrain(candidato).and(
		//				q.descend("dataInscricao").constrain(data) );
		if (q.equals(inscricaoTmp)) {
			q.descend("participacao").descend("atividade").descend("nome");
			List<String> resultado = q.execute();
			if (resultado.size()>0)
				return resultado;
		}

		return null;
	}
}
