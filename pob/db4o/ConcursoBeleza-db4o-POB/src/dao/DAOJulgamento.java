package dao;

import java.util.List;

import com.db4o.query.Query;

import modelo.Julgamento;

public class DAOJulgamento extends DAO<Julgamento>{

	public Julgamento read (Object chave) {
		String quesito = (String) chave;
		Query q = manager.query();
		q.constrain(Julgamento.class);
		q.descend("quesito").constrain(quesito);
		List<Julgamento> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
}
