package dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.db4o.query.Query;

import modelo.Atividade;
import modelo.Evento;
import modelo.Participacao;

public class DAOAtividade extends DAO<Atividade>{

	public Atividade read (Object chave) {
		String nome = (String) chave;
		Query q = manager.query();
		q.constrain(Atividade.class);
		q.descend("nome").constrain(nome);
		List<Atividade> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
	
	public Atividade buscarPeloNomeEEvento(String nomeAtividade, String nomeEvento ) {
		String nome = (String) nomeAtividade;
		Query q = manager.query();
		q.constrain(Atividade.class);
		q.descend("nome").constrain(nome).and(
			q.descend("evento").descend("nome").constrain(nomeEvento));
		List<Atividade> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
	
	public List<String> buscarParticipantes(String nomeAtividade, String nomeEvento) {
		Atividade atividade = buscarPeloNomeEEvento(nomeAtividade,nomeEvento);
		Query q = manager.query();
		q.constrain(Atividade.class);
		 if (q.equals(atividade) ) {
			  q.descend("participacao").descend("inscricao").descend("candidato").descend("nome");
			  List<String> resultados = q.execute();
			  return resultados;
		 } else 
			 	return null;
		
	}
	
	public List<Atividade> buscarAtividadesPeloDia(LocalDate dia){
		Query q = manager.query();
		q.constrain(Atividade.class);
		q.descend("horario").constrain(dia);
		List<Atividade> atividades = q.execute();
		if (atividades.size() > 0) {
			return atividades;
		} 
		else 
			return null;		
	}
	
	public List<String> buscarJulgadores(String nomeAtividade, String nomeEvento) {
		Atividade atividade = buscarPeloNomeEEvento(nomeAtividade,nomeEvento);
		Query q = manager.query();
		q.constrain(Atividade.class);
		 if (q.equals(atividade) ) {
			  q.descend("pontuacao").descend("juiz").descend("nome");
			  List<String> resultados = q.execute();
			  return resultados;
		 } else 
			 	return null;
		
	}
}
