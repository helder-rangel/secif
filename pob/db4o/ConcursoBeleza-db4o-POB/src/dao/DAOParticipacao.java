package dao;

import java.util.List;

import com.db4o.query.Query;

import modelo.Participacao;

public class DAOParticipacao extends DAO<Participacao>{

	public Participacao read (Object chave) {
		boolean checkin = (boolean) chave;
		Query q = manager.query();
		q.constrain(Participacao.class);
		q.descend("checkin").constrain(checkin);
		List<Participacao> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
}
