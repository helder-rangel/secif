package dao;

import java.time.LocalDate;
import java.util.List;

import com.db4o.query.Query;

import modelo.Candidato;

public class DAOCandidato  extends DAO<Candidato>{
	public Candidato read (Object chave) {
		String matricula = (String) chave;
		Query q = manager.query();
		q.constrain(Candidato.class);
		q.descend("matricula").constrain(matricula);
		List<Candidato> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
	
	public List<Candidato> buscarPorCurso (String curso) {
		Query q = manager.query();
		q.constrain(Candidato.class);
		q.descend("curso").constrain(curso);
		List<Candidato> resultado = q.execute(); 
		return resultado;
	}
	
	public Candidato buscarCandidato(Candidato candidato) {
		Query q = manager.query();
		q.constrain(Candidato.class);
		q.equals(candidato);
		Candidato resultado = (Candidato) q.execute();		
		return resultado;
	}
	
	public void buscarAtividades(String matricula, LocalDate data) {
		//TODO
	}
}

