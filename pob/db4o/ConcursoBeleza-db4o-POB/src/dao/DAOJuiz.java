package dao;

import java.util.List;

import com.db4o.query.Query;

import modelo.Juiz;
import modelo.Pessoa;

public class DAOJuiz extends DAO<Juiz>{
	
	public Juiz read (Object chave) {
		
//		String instituicao = (String) chave;
		String cpf = (String) chave;
		Query q = manager.query();
		q.constrain(Juiz.class);
//		q.descend("instituicao").constrain(instituicao);
//		q.descend("cpf").constrain(cpf);
		q.descend("cpf").constrain(cpf);
		List<Juiz> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
	
	public Juiz buscarPeloNome (String nomeJuiz) {
		
		String nome = (String) nomeJuiz;
		Query q = manager.query();
		q.constrain(Juiz.class);
		q.descend("nome").constrain(nome);
		List<Juiz> resultados = q.execute();
		if (resultados.size()>0)
			return resultados.get(0);
		else
			return null;
	}
	
}
