package aplicacaoConsole;
/**********************************
 * IFPB - Curso Superior de Tec. em Sist. para Internet
 * POB - Persistencia de Objetos
 * Prof. Fausto Ayres
 *
 */

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import fachada.Fachada;
import modelo.Atividade;
import modelo.Candidato;
import modelo.Evento;
import modelo.Inscricao;
import modelo.Juiz;
import modelo.Julgamento;
import modelo.Participacao;
import modelo.Pessoa;


public class Teste {

	public Teste(){

		Fachada.inicializar();

		try {
//			cadastrar();
		}catch(Exception e) {
			e.printStackTrace();
		}


		try {
//			apagar();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		listar();

		try {
//			atualizar();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Fachada.finalizar();
		System.out.println("fim do programa");
	}


	public void cadastrar(){

		Atividade atividade1,atividade2,atividade3;
		Candidato candidato1,candidato2,candidato3;
		Evento evento1,evento2,evento3;
		Inscricao inscricao1,inscricao2,inscricao3;
		Juiz juiz1,juiz2,juiz3;
		Julgamento julgamento;
		Participacao participacao1,participacao2,participacao3;
		Pessoa pessoa1,pessoa2,pessoa3;
		ArrayList<Julgamento> julgamentos = new ArrayList<Julgamento>();

		System.out.println("cadastrando todos os objetos...");

		try {
			julgamento=Fachada.cadastrarJulgamento(4.0,"Desenvoltura");
			julgamento=Fachada.cadastrarJulgamento(6.1,"Beleza");
			julgamento=Fachada.cadastrarJulgamento(9.1,"Simpatia");
			julgamentos.add(julgamento);

			atividade1=Fachada.cadastrarAtividade("Beleza","Concurso de beleza do secif 2019",1,256,LocalDate.now());
			atividade2=Fachada.cadastrarAtividade("Volei","Competicao de volei do secif 2019",1,256,LocalDate.now());
			atividade3=Fachada.cadastrarAtividade("Futebol de salão ","Competicao de futsal do secif 2019",5,15,LocalDate.now());
			atividade3=Fachada.buscarAtividade(new Atividade("Futebol de salão ","Competicao de futsal do secif 2019",5,15,LocalDate.now()) );
			System.out.println(atividade3);

			candidato1=Fachada.cadastrarCandidato("Jose Xico","jose.xico@email.com.br","9809889808","988776655","Eletrotecnica","20131720580");
			candidato2=Fachada.cadastrarCandidato("Maria Josefina ","MariaJosefina@email.com.br","9809812318","988776455","Edificacoes","20181920580");
			candidato3=Fachada.cadastrarCandidato("Marcos Neto ","marcos.nett@email.com.br","9809812218","9487764312","Controle Ambiental","20161960510");
			System.out.println(candidato2);

			inscricao1=Fachada.cadastrarInscricao(atividade1,false,candidato2,LocalDate.now() );
			inscricao2=Fachada.cadastrarInscricao(atividade2,false,candidato1,LocalDate.now() );
			inscricao3=Fachada.cadastrarInscricao(atividade3,false,candidato3,LocalDate.now() );
			System.out.println(inscricao1);

		} catch (Exception e) {
			e.printStackTrace() ;
		}


		try {
			evento1=Fachada.cadastrarEvento("SECIT","2019");
			evento2=Fachada.cadastrarEvento("Secif","2019");
			evento3=Fachada.cadastrarEvento("If.Tech","2019");
			System.out.println(evento1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			juiz1=Fachada.cadastrarJuiz("Beltrano","bletrano@email.com","312.421.786-21","88888888",true,"Pio XI");
			juiz2=Fachada.cadastrarJuiz("Sicrano","sicrano@email.com","333.112.231.99","88888888",false,"IFPB - João Pessoa");
			juiz3=Fachada.cadastrarJuiz("Antonio Silva Sales","toinss@email.com","120.212.900-00","88888888",true,"UFPB - Campus V");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		try {
			participacao1=Fachada.cadastrarParticipacao(false,false,0);
			participacao2=Fachada.cadastrarParticipacao(true,true,10);
//			participacao3=Fachada.cadastrarParticipacao(true,false,5); // TODO -> Deu erro pois já existia, necessário checar modelagem
			System.out.println(participacao1);

			pessoa1=Fachada.cadastrarPessoa("joao","asa@aaa","9809889808","988776655");
			pessoa2=Fachada.cadastrarPessoa("Tayna Sales","tata@email.com","9987543808","999279655");
			pessoa3=Fachada.cadastrarPessoa("Xico Cesar","xico.czr@aaa","9123489808","999772255");	
			System.out.println(pessoa1);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}	

	public void atualizar()  throws Exception{

		Atividade atividade,atividade2,atividade3;
		Candidato candidato,candidato3;
		Evento evento;
		Inscricao inscricao;
		Juiz juiz;
		Julgamento julgamento;
		Participacao participacao;
		Pessoa pessoa;


		System.out.println("Atualizando o nome de joao para Josefino...");
		//ATUALIZAR OBJETOS | UPDATE OBJECTS

		 Fachada.atualizarNomePessoa("Xico Cesar", "Xico César"); 

		 Fachada.atualizarEmailPessoa("Xico Cesar", "xico.czr@aaa", "xico.czr@email.com"); 

		 Fachada.atualizarTelefonePessoa("Xico Cesar", "999772255", "991772255"); 

		 Fachada.atualizarCpfPessoa("Xico Cesar", "9123489808", "9923489800"); 


		 Fachada.atualizarMatriculaCandidato("20161960510", "20161960511"); 
			
		 Fachada.atualizarCursoCandidato("20161960511", "Controle Ambiental", "Controle_Ambiental"); 
			

		 Fachada.atualizarInstituicaoJuiz("120.212.900-00", "UFPB - Campus V", "IFPB - João Pessoa"); 
			
		 Fachada.atualizarExternoJuiz("120.212.900-00", true, false);
			   
			
		 Fachada.atualizarNomeAtividade ("Volei", "Voleibol");
			
		 Fachada.atualizarDescricaoAtividade ("Voleibol" ,"Competicao de voleibol do secif 2019"); 
			
		 Fachada.atualizarNumMaxAtividade ("Voleibol" ,6); 
			
		 Fachada.atualizarNumMinAtividade ("Voleibol" ,6); 
			
		 Fachada.atualizarHorarioAtividade ("Voleibol" ,LocalDate.of(2019, 9, 15) ); 
			
		 ArrayList<Julgamento> julgamentos1 = new ArrayList<Julgamento>();
		 julgamentos1.add(new Julgamento(7,"Jogo1"));
		 Fachada.atualizarListaJulgamentosAtividade ("Voleibol" ,  julgamentos1 );
			
			 
			
		 Fachada.atualizarNomeEvento ("If.Tech", "If_Tech"); 
			
		 Fachada.atualizarAnoEvento ("If_Tech", "2019", "2019.2");
			
		 atividade2=Fachada.buscarAtividadePeloNome("Voleibol");
		 atividade3=Fachada.buscarAtividadePeloNome("Futebol de salão ");
		 candidato3=Fachada.buscarCandidatoPelaMatricula("20161960510");
		 Fachada.atualizarCompeticaoInscricao (candidato3, atividade3, atividade2); 
			  
			
		 Fachada.atualizarCertificadoInscricao (candidato3, false, true); 
			
		 Fachada.atualizarDataInscricao (candidato3, LocalDate.of(2019, 9, 5)); 
			
			
		 Fachada.atualizarQuesitoJulgamento("Beleza", "Beleza_no_Palco");
			
		 Fachada.atualizarNotaJulgamento("Beleza_no_Palco", 7); 
			
			
		 Fachada.atualizarCheckinParticipacao (true, false);
			
		 Fachada.atualizarCertificadoParticipacao (false, false);   
			
		 Fachada.atualizarPontuacaoParticipacao (false, 2);   
		

	}	

	public void listar() {

		System.out.println("Listando todos os objetos do banco");

		String atividades = Fachada.listarAtividades();
		System.out.println(atividades);

		String candidatos = Fachada.listarCandidatos();
		System.out.println(candidatos);

		String eventos = Fachada.listarEventos();
		System.out.println(eventos);

		String inscricoes = Fachada.listarInscricoes();
		System.out.println(inscricoes);			

		String juizes = Fachada.listarJuizes();
		System.out.println(juizes);

		String julgamentos = Fachada.listarJulgamentos();
		System.out.println(julgamentos);

		String participacoes = Fachada.listarParticipacoes();
		System.out.println(participacoes);

		String pessoas = Fachada.listarPessoas();
		System.out.println(pessoas);

	}

	public void apagar()  throws Exception{

		
		Candidato candidato = Fachada.buscarCandidatoPelaMatricula("20131720580");

		Fachada.apagarAtividade("Futebol de salão ");
		Fachada.apagarEvento("SECIT");
		Fachada.apagarInscricao(candidato);//é necessário passar um objeto do tipo candidato
		Fachada.apagarCandidato("20131720580");
		Fachada.apagarJuiz("312.421.786-21"); 
		Fachada.apagarJulgamento("Desenvoltura");
		Fachada.apagarParticipacao(false);
		Fachada.apagarPessoa("joao");
	}	

	//=================================================
	public static void main(String[] args) {
		try {
			new Teste();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}


