package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Juiz;

public class JuizApaga extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldCpf;
	
	/**
	 * Create the application.
	 */
	public JuizApaga() {
		initialize();
	}

	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Juiz | Apagar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNomeDaAtividade = new JLabel("CPF do juiz");
		lblNomeDaAtividade.setBounds(12, 12, 141, 15);
		getContentPane().add(lblNomeDaAtividade);
		
		textFieldCpf = new JTextField();
		textFieldCpf.setBounds(22, 39, 114, 19);
		getContentPane().add(textFieldCpf);
		textFieldCpf.setColumns(10);
		
		JButton btnApagarAtividade = new JButton("Apagar atividade");
		btnApagarAtividade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cpf = textFieldCpf.getText();
				try {
					Juiz apagar = Fachada.apagarJuiz(cpf);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnApagarAtividade.setBounds(12, 76, 191, 25);
		getContentPane().add(btnApagarAtividade);
		
		JTextArea textAreaResultado = new JTextArea();
		textAreaResultado.setBounds(22, 265, 174, 28);
		getContentPane().add(textAreaResultado);
		setVisible(true);
	}

}
