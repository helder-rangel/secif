package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Candidato;
import javax.swing.JTextArea;

public class CandidatoApaga extends JFrame {

	private JPanel contentPane;
	private JTextField txtCandidato;
	
	/**
	 * Create the application.
	 */
	public CandidatoApaga() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {

		setTitle("Candidato | Apagar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblCandidato = new JLabel("Candidato");
		lblCandidato.setBounds(12, 12, 120, 15);
		getContentPane().add(lblCandidato);
		
		txtCandidato = new JTextField();
		txtCandidato.setText("candidato");
		txtCandidato.setBounds(22, 39, 114, 19);
		getContentPane().add(txtCandidato);
		txtCandidato.setColumns(10);
		
		JButton btnApagarCandidato = new JButton("Apagar candidato");
		btnApagarCandidato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String matricula = txtCandidato.getText();
				
				try {
					Candidato apagCand = Fachada.apagarCandidato(matricula);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnApagarCandidato.setBounds(32, 80, 169, 25);
		getContentPane().add(btnApagarCandidato);
		
		JTextArea txtrCandidatoresultado = new JTextArea();
		txtrCandidatoresultado.setText("candidatoResultado");
		txtrCandidatoresultado.setBounds(32, 262, 169, 15);
		getContentPane().add(txtrCandidatoresultado);
		
		
	}
}
