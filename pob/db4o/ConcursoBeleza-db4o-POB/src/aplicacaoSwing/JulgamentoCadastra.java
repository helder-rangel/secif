package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Julgamento;

public class JulgamentoCadastra extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldAnoQuesito;
	private JTextField textFieldNota;
	private JLabel lblNota;
	private JLabel lblQuesito;
	private JButton btnCadastrarJulgamento;
	/**
	 * Create the application.
	 */
	public JulgamentoCadastra() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {

		setTitle("Julgamento | Cadastrar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNota = new JLabel("Nota");
		lblNota.setBounds(26, 12, 70, 15);
		getContentPane().add(lblNota);
		
		textFieldNota = new JTextField();
		textFieldNota.setBounds(36, 36, 114, 19);
		getContentPane().add(textFieldNota);
		textFieldNota.setColumns(10);
		
		JLabel lblQuesito = new JLabel("Quesito");
		lblQuesito.setBounds(26, 82, 70, 15);
		getContentPane().add(lblQuesito);
		
		textFieldAnoQuesito = new JTextField();
		textFieldAnoQuesito.setBounds(36, 109, 114, 19);
		getContentPane().add(textFieldAnoQuesito);
		textFieldAnoQuesito.setColumns(10);
		
		JButton btnCadastrarJulgamento = new JButton("Cadastrar Julgamento");
		btnCadastrarJulgamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double nota = Double.parseDouble(textFieldNota.getText());
				String quesito = textFieldAnoQuesito.getText();
				try {
					Julgamento cadastro = Fachada.cadastrarJulgamento(nota,quesito);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCadastrarJulgamento.setBounds(12, 158, 191, 25);
		getContentPane().add(btnCadastrarJulgamento);
		setVisible(true);
	}

}
