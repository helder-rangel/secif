package aplicacaoSwing;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import fachada.Fachada;

public class JulgamentoLista extends JFrame {

	private JPanel contentPane;
	private JButton btnListarJulgamentos;
	
	/**
	 * Create the application.
	 */
	public JulgamentoLista() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Secif | Listagem de Julgamentos");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setVisible(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
				
		JTextArea textArea = new JTextArea();
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(30,38,396,106);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scroll);
		
		btnListarJulgamentos = new JButton("Listar Julgamentos");
		btnListarJulgamentos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					textArea.setText(Fachada.listarJulgamentos());
				}
				catch(Exception erro) {
					JOptionPane.showMessageDialog(null,erro.getStackTrace());
				}
			}
		});
		
		btnListarJulgamentos.setBounds(30, 206, 139, 25);
		contentPane.add(btnListarJulgamentos);
	}

}
