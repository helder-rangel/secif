package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Participacao;

public class InscricaoAtualiza extends JFrame {

	private JPanel contentPane;
	private JTextField txtMatricula;
	private JTextField txtPresenca;
	private JTextField txtNovaatividade;
	
	/**
	 * Create the application.
	 */
	public InscricaoAtualiza() {
		initialize();
	}

	/**
	 * Create the frame.
	 */
	public void initialize() {
			
		setTitle("Inscrição | Atualizar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblMatrcula = new JLabel("Matrícula");
		lblMatrcula.setBounds(12, 12, 118, 15);
		getContentPane().add(lblMatrcula);
		
		txtMatricula = new JTextField();
		txtMatricula.setText("matricula");
		txtMatricula.setBounds(12, 39, 114, 19);
		getContentPane().add(txtMatricula);
		txtMatricula.setColumns(10);
		
		txtPresenca = new JTextField();
		txtPresenca.setText("sim ou nao");
		txtPresenca.setBounds(16, 100, 114, 19);
		getContentPane().add(txtPresenca);
		txtPresenca.setColumns(10);
		
		JLabel lblAtividadeAtual = new JLabel("Participante presente");
		lblAtividadeAtual.setBounds(12, 73, 165, 15);
		getContentPane().add(lblAtividadeAtual);
		
		JLabel lblNovaAtividade = new JLabel("Nova atividade");
		lblNovaAtividade.setBounds(12, 128, 118, 19);
		getContentPane().add(lblNovaAtividade);
		
		txtNovaatividade = new JTextField();
		txtNovaatividade.setText("novaAtividade");
		txtNovaatividade.setBounds(16, 159, 114, 19);
		getContentPane().add(txtNovaatividade);
		txtNovaatividade.setColumns(10);
		
		JButton btnAtualizarInscrio = new JButton("Atualizar inscrição");
		btnAtualizarInscrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String matricula = txtMatricula.getText();
				Boolean check = Boolean.parseBoolean(txtPresenca.getText());
				
				try {
					Participacao atualiza = Fachada.atualizarInscricaoParticipacao(check,matricula);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				
			}
		});
		btnAtualizarInscrio.setBounds(12, 191, 165, 25);
		getContentPane().add(btnAtualizarInscrio);
		
	
	
	}
}
