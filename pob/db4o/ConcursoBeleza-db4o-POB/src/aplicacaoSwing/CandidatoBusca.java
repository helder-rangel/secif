package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Candidato;

public class CandidatoBusca extends JFrame {
	private JTextField txtCampo1;
	private JTextField txtCampo2;
	public CandidatoBusca() {
		setTitle("Candidato | Buscas");
		initialize();
	}

	private void initialize() {
		getContentPane().setLayout(null);
		
		JLabel lblCampoDeEntrada = new JLabel("Campo de Entrada:");
		lblCampoDeEntrada.setBounds(12, 12, 150, 15);
		getContentPane().add(lblCampoDeEntrada);
		setVisible(true);
		
		txtCampo1 = new JTextField();
		txtCampo1.setBounds(170, 12, 114, 19);
		getContentPane().add(txtCampo1);
		txtCampo1.setColumns(10);
		
		JLabel lblCampoDeEntrada_1 = new JLabel("Campo de Entrada 2:");
		lblCampoDeEntrada_1.setBounds(12, 36, 150, 15);
		getContentPane().add(lblCampoDeEntrada_1);
		
		txtCampo2 = new JTextField();
		txtCampo2.setColumns(10);
		txtCampo2.setBounds(170, 36, 114, 19);
		getContentPane().add(txtCampo2);
		
		JTextArea txtrSaida = new JTextArea();
		txtrSaida.setText("Saida");
		txtrSaida.setBounds(12, 71, 272, 161);
		getContentPane().add(txtrSaida);
		
		JButton btnBuscarPorCurso = new JButton("Buscar Pelo Curso");
		btnBuscarPorCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String saida = Fachada.buscarCandidatoPorCurso(txtCampo1.getText() );
					txtrSaida.setText(saida);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
			}
		});
		
		btnBuscarPorCurso.setBounds(312, 7, 189, 25);
		getContentPane().add(btnBuscarPorCurso);
		
		JButton btnBuscarPeloDia = new JButton("Buscar pela Matricula");
		btnBuscarPeloDia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Candidato c = Fachada.buscarCandidatoPelaMatricula(txtCampo1.getText());
					String saida = c.toString();
					txtrSaida.setText(saida);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
			}
		});
		btnBuscarPeloDia.setBounds(312, 36, 189, 25);
		getContentPane().add(btnBuscarPeloDia);
		
		JButton btnBuscarAtividades = new JButton("Buscar Atividades");
		btnBuscarAtividades.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String saida = Fachada.buscarCandidatoAtividades(txtCampo1.getText(), txtCampo2.getText());
					txtrSaida.setText(saida);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
			}
		});
		
		btnBuscarAtividades.setBounds(312, 66, 189, 25);
		getContentPane().add(btnBuscarAtividades);
		
		JTextArea txtrParametros = new JTextArea();
		txtrParametros.setText("Curso\nCampo 1: Nome do Curso\nMatricula\nCampo 1: Num. da Matricula\nAtividades\nCampo 1: Num. da Matricula\nCampo 2: Data da Inscricao\n\n\n");
		txtrParametros.setBounds(312, 103, 176, 110);
		getContentPane().add(txtrParametros);
		
		
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(267, 71, 17, 156);
		getContentPane().add(scrollBar);
		setBounds(100, 100, 528, 300);
		setVisible(true);
		
		}

}
