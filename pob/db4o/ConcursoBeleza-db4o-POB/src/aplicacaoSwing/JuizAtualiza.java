package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Juiz;

public class JuizAtualiza extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldCpfJuiz;
	private JTextField textFieldInstituicao;
	private JTextField textFieldNovaInstituicao;
	private JLabel lblCPF;
	private JLabel lblInstitu;
	private JLabel lblNova;
	
	
	/**
	 * Create the application.
	 */
	public JuizAtualiza() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {

		setTitle("Juiz | Alterar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblCPF = new JLabel("Cpf do Juiz");
		lblCPF.setBounds(26, 12, 124, 19);
		getContentPane().add(lblCPF);
		
		textFieldCpfJuiz = new JTextField();
		textFieldCpfJuiz.setBounds(36, 36, 114, 19);
		getContentPane().add(textFieldCpfJuiz);
		textFieldCpfJuiz.setColumns(10);
		
		JLabel lblInstitu = new JLabel("Instituição");
		lblInstitu.setBounds(46, 67, 124, 15);
		getContentPane().add(lblInstitu);
		
		textFieldInstituicao = new JTextField();
		textFieldInstituicao.setBounds(36, 155, 114, 19);
		getContentPane().add(textFieldInstituicao);
		textFieldInstituicao.setColumns(10);
		
		JLabel lblNova = new JLabel("Nova Instituição");
		lblNova.setBounds(36, 128, 124, 15);
		getContentPane().add(lblNova);
		
		textFieldNovaInstituicao = new JTextField();
		textFieldNovaInstituicao.setBounds(36, 109, 114, 19);
		getContentPane().add(textFieldNovaInstituicao);
		textFieldNovaInstituicao.setColumns(10);
		
		JButton btnCadastrarEvemto = new JButton("Cadastrar evento");
		btnCadastrarEvemto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cpf = textFieldCpfJuiz.getText();
				String instituicao = textFieldInstituicao.getText();
				String nova = textFieldNovaInstituicao.getText();
				try {
					Juiz alerar = Fachada.atualizarInstituicaoJuiz(cpf,instituicao,nova);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCadastrarEvemto.setBounds(26, 224, 163, 25);
		getContentPane().add(btnCadastrarEvemto);
		setVisible(true);
	}

}
