package aplicacaoSwing;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Atividade;

import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollBar;

public class AtividadeBuscas extends JFrame {
	private JTextField txtCampo1;
	private JTextField txtCampo2;
	public AtividadeBuscas() {
		setTitle("Atividade | Buscas");
		initialize();
	}

	private void initialize() {
		getContentPane().setLayout(null);
		
		JLabel lblCampoDeEntrada = new JLabel("Campo de Entrada:");
		lblCampoDeEntrada.setBounds(12, 12, 113, 15);
		getContentPane().add(lblCampoDeEntrada);
		
		txtCampo1 = new JTextField();
		txtCampo1.setBounds(135, 10, 114, 19);
		getContentPane().add(txtCampo1);
		txtCampo1.setColumns(10);
		
		JLabel lblCampoDeEntrada_1 = new JLabel("Campo de Entrada 2:");
		lblCampoDeEntrada_1.setBounds(12, 36, 126, 15);
		getContentPane().add(lblCampoDeEntrada_1);
		
		txtCampo2 = new JTextField();
		txtCampo2.setColumns(10);
		txtCampo2.setBounds(135, 34, 114, 19);
		getContentPane().add(txtCampo2);
		
		JTextArea txtrSaida = new JTextArea();
		txtrSaida.setText("Saida");
		txtrSaida.setBounds(12, 71, 272, 187);
		getContentPane().add(txtrSaida);
		
		JButton btnBuscarnomeEEvento = new JButton("Buscar Nome e Evento");
		btnBuscarnomeEEvento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Atividade resultado = Fachada.buscarAtividadePeloNomeEEvento(txtCampo1.getText(), txtCampo2.getText() );
					String saida = resultado.toString();
					txtrSaida.setText(saida);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
			}
		});
		
		btnBuscarnomeEEvento.setBounds(312, 7, 176, 25);
		getContentPane().add(btnBuscarnomeEEvento);
		
		JButton btnBuscarPeloDia = new JButton("Buscar pelo Dia");
		btnBuscarPeloDia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String saida = Fachada.buscarAtividadePeloDia(txtCampo1.getText() );
					txtrSaida.setText(saida);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
			}
		});
		btnBuscarPeloDia.setBounds(312, 36, 176, 25);
		getContentPane().add(btnBuscarPeloDia);
		
		JButton btnBuscarParticipantes = new JButton("Buscar Participantes");
		btnBuscarParticipantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String saida = Fachada.buscarAtividadeParticipantes(txtCampo1.getText(), txtCampo2.getText() );
					txtrSaida.setText(saida);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
			}
		});
		
		btnBuscarParticipantes.setBounds(312, 66, 176, 25);
		getContentPane().add(btnBuscarParticipantes);
		
		JTextArea txtrParametros = new JTextArea();
		txtrParametros.setText("Evento, participantes\ne Julgadores\nCampo 1: Nome da Atividade \"\nCampo 2: Nome do Evento\"\n\nDia\nCampo 1: Dia da Atividade\nFormato: dd/mm/aa");
		txtrParametros.setBounds(312, 135, 176, 123);
		getContentPane().add(txtrParametros);
		
		
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(267, 71, 17, 156);
		getContentPane().add(scrollBar);
		
		JButton btnBuscarJulgadores = new JButton("Buscar Julgadores");
		btnBuscarJulgadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String saida = Fachada.buscarAtividadeJulgadores(txtCampo1.getText(), txtCampo2.getText() );
					txtrSaida.setText(saida);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
			}
		});
		btnBuscarJulgadores.setBounds(312, 98, 176, 25);
		getContentPane().add(btnBuscarJulgadores);
		setBounds(100, 100, 528, 300);
		setVisible(true);
		
		}
}
