package aplicacaoSwing;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Atividade;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class AtividadeAtualiza extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtAtualizarnome;
	private JTextField txtAtualizarhorario;
	private JTextField txtAtualizarnummin;
	private JTextField txtAtualizarnummax;
	private JTextField txtAlterardescricao;
	private JTextField textFieldNomeAtiv;

	/**
	 * Create the application.
	 */
	public AtividadeAtualiza() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {
		getContentPane().setLayout(null);
		setBounds(100, 100, 528, 300);
		
		txtAtualizarnome = new JTextField();
		txtAtualizarnome.setBounds(155, 56, 130, 19);
		getContentPane().add(txtAtualizarnome);
		txtAtualizarnome.setColumns(10);
		
		txtAtualizarhorario = new JTextField();
		txtAtualizarhorario.setToolTipText("dd/mm/aa");
		txtAtualizarhorario.setBounds(155, 86, 130, 19);
		getContentPane().add(txtAtualizarhorario);
		txtAtualizarhorario.setColumns(10);
		
		txtAtualizarnummin = new JTextField();
		txtAtualizarnummin.setBounds(155, 129, 130, 19);
		getContentPane().add(txtAtualizarnummin);
		txtAtualizarnummin.setColumns(10);
		
		txtAtualizarnummax = new JTextField();
		txtAtualizarnummax.setBounds(155, 170, 130, 19);
		getContentPane().add(txtAtualizarnummax);
		txtAtualizarnummax.setColumns(10);
		
		txtAlterardescricao = new JTextField();
		txtAlterardescricao.setBounds(155, 201, 130, 19);
		getContentPane().add(txtAlterardescricao);
		txtAlterardescricao.setColumns(10);
		setTitle("Atividade | Atualizar");
		getContentPane().setLayout(null);
		
		textFieldNomeAtiv = new JTextField();
		textFieldNomeAtiv.setColumns(10);
		textFieldNomeAtiv.setBounds(155, 26, 130, 19);
		getContentPane().add(textFieldNomeAtiv);
		setVisible(true);
		
		String nome = textFieldNomeAtiv.getText();		
		String novoNome = txtAtualizarnome.getText();
		String novaDescricao = txtAlterardescricao.getText();
		
		
		
		
		JButton btnNewButton = new JButton("Atualizar Nome");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Fachada.atualizarNomeAtividade(nome, novoNome);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e1.getStackTrace());
				}
			}
		});
		btnNewButton.setBounds(329, 51, 155, 25);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Atualizar Horario");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String novoHora = txtAtualizarhorario.getText();
				LocalDate horario;
				DateTimeFormatter formatado = DateTimeFormatter.ofPattern("dd/MM/yy");
				horario =  LocalDate.parse(novoHora, formatado);
				
				try {
					Fachada.atualizarHorarioAtividade(nome,horario);
				} catch (Exception eH) {
					JOptionPane.showMessageDialog(null,eH.getStackTrace());
				}
				
			}
		});
		btnNewButton_1.setBounds(329, 87, 155, 25);
		getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Atualizar Num. Min");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double novoMin = Double.parseDouble(txtAtualizarnummin.getText());
					Fachada.atualizarNumMinAtividade(nome, novoMin);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,e.getStackTrace());
				}
			}
		});
		btnNewButton_2.setBounds(329, 123, 155, 25);
		getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Atualizar Num. max");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					double novoMax = Double.parseDouble(txtAtualizarnummax.getText());
					Fachada.atualizarNumMaxAtividade(nome, novoMax);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton_3.setBounds(329, 159, 155, 25);
		getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Atualizar Descricao");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Fachada.atualizarDescricaoAtividade(nome, novaDescricao);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_4.setBounds(329, 195, 155, 25);
		getContentPane().add(btnNewButton_4);
		
		JLabel lblNumMin = new JLabel("Novo Num. Min");
		lblNumMin.setBounds(24, 132, 113, 14);
		getContentPane().add(lblNumMin);
		
		JLabel lblNumMax = new JLabel("Novo Num Max");
		lblNumMax.setBounds(24, 173, 113, 14);
		getContentPane().add(lblNumMax);
		
		JLabel lblHorario = new JLabel("NovoHorario");
		lblHorario.setBounds(24, 89, 113, 14);
		getContentPane().add(lblHorario);
		
		JLabel lblDescricao = new JLabel("Nova Descricao");
		lblDescricao.setBounds(24, 203, 113, 15);
		getContentPane().add(lblDescricao);
		
		JLabel lblNovoNome = new JLabel("Novo Nome");
		lblNovoNome.setBounds(24, 58, 113, 15);
		getContentPane().add(lblNovoNome);
		
		JLabel lblNomeAtividade = new JLabel("Nome Atividade");
		lblNomeAtividade.setBounds(24, 28, 113, 15);
		getContentPane().add(lblNomeAtividade);
		
		
	}
}
