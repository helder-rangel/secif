package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Candidato;
import modelo.Inscricao;

public class InscricaoApaga extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldInscricao;
	
	/**
	 * Create the application.
	 */
	public InscricaoApaga() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {
		

		setTitle("Inscrição | Apagar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblInscricao = new JLabel("Inscricao");
		lblInscricao.setBounds(12, 12, 70, 15);
		getContentPane().add(lblInscricao);
		
		textFieldInscricao = new JTextField();
		textFieldInscricao.setBounds(22, 30, 114, 19);
		getContentPane().add(textFieldInscricao);
		textFieldInscricao.setColumns(10);
		
		JButton btnApagarInscrio = new JButton("Apagar inscrição");
		btnApagarInscrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inscricao = textFieldInscricao.getText();
				Candidato busca;
				
				try {
					busca = Fachada.buscarCandidatoPelaMatricula(inscricao);
					Inscricao apagaInscricao = Fachada.apagarInscricao(busca);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnApagarInscrio.setBounds(22, 83, 164, 25);
		getContentPane().add(btnApagarInscrio);
		
		

	}

}
