package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Evento;

public class EventoCadastra extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNomeEvento;
	private JTextField textFieldAnoEvento;
	
	
	/**
	 * Create the application.
	 */
	public EventoCadastra() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {

		setTitle("Evento | Cadastrar");
		setBounds(100, 100, 225, 331);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(26, 12, 70, 15);
		getContentPane().add(lblNome);
		
		textFieldNomeEvento = new JTextField();
		textFieldNomeEvento.setBounds(36, 36, 114, 19);
		getContentPane().add(textFieldNomeEvento);
		textFieldNomeEvento.setColumns(10);
		
		JLabel lblAno = new JLabel("Ano");
		lblAno.setBounds(26, 82, 70, 15);
		getContentPane().add(lblAno);
		
		textFieldAnoEvento = new JTextField();
		textFieldAnoEvento.setBounds(36, 109, 114, 19);
		getContentPane().add(textFieldAnoEvento);
		textFieldAnoEvento.setColumns(10);
		
		JButton btnCadastrarEvemto = new JButton("Cadastrar evento");
		btnCadastrarEvemto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeEvento = textFieldNomeEvento.getText();
				String anoEvento = textFieldAnoEvento.getText();
				try {
					Evento cadastro = Fachada.cadastrarEvento(nomeEvento,anoEvento);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCadastrarEvemto.setBounds(26, 158, 163, 25);
		getContentPane().add(btnCadastrarEvemto);
	}

}
