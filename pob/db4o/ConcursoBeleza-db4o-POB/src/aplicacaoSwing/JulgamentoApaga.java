package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Julgamento;

public class JulgamentoApaga extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNomeQuesito;
	private JButton btnApagarJulgamento;
	
	/**
	 * Create the application.
	 */
	public JulgamentoApaga() {
		initialize();
	}

	/**
	 * Create the frame.
	 */
	public void initialize() {
		
		setTitle("Julgamento | Apagar");
		setBounds(100, 100, 225, 331);
		JLabel lblJulgamentoApaga = new JLabel("Apagar Julgamento");
		lblJulgamentoApaga.setBounds(12, 12, 141, 15);
		getContentPane().add(lblJulgamentoApaga);
		
		textFieldNomeQuesito = new JTextField();
		textFieldNomeQuesito.setBounds(22, 39, 114, 19);
		getContentPane().add(textFieldNomeQuesito);
		textFieldNomeQuesito.setColumns(10);
		
		JButton btnApagarJulgamento = new JButton("Apagar julgamento");
		btnApagarJulgamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String quesito = textFieldNomeQuesito.getText();
				try {
					Julgamento apagar = Fachada.apagarJulgamento(quesito);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnApagarJulgamento.setBounds(12, 76, 191, 25);
		getContentPane().add(btnApagarJulgamento);
		
		JTextArea textAreaResultado = new JTextArea();
		textAreaResultado.setBounds(22, 265, 174, 28);
		getContentPane().add(textAreaResultado);
		setVisible(true);
	}

}
