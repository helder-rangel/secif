package aplicacaoSwing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import fachada.Fachada;
import modelo.Candidato;

public class CandidatoCadastro extends JFrame {

	private JPanel contentPane;
	private JFrame frmCadastro;
	private JLabel label1;
	private JTextField jtextfieldnome;
	private JTextField jtextfieldemail;
	private JButton button1;
	private JScrollPane scrollPane;
	private JButton button2;
	private JLabel label2;
	private JLabel lblEmail;
	private JLabel lblNewLabel;
	private JTextField jtextfieldcpf;
	private JLabel lbltelefone;
	private JTextField jtextfieldtelefone;
	private JLabel lblCurso;
	private JTextField jtextfieldcurso;
	private JTextField jtextfieldmatricula;
	private JLabel lblMatrcula;
	private JTextField textFieldtexto;
	private DefaultListModel<String> model;
	private JList<String> list;

	/**
	 * Create the application.
	 */
	public CandidatoCadastro() {
		initialize();
	}
	/**
	 * Create the frame.
	 */
	public void initialize() {
		frmCadastro = new JFrame();
		frmCadastro.setTitle("Candidato | Cadastrar");
		frmCadastro.setBounds(100, 100, 225, 331);
		frmCadastro.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmCadastro.getContentPane().setLayout(null);
				
		label1 = new JLabel("nome:");
		label1.setBounds(10, 33, 46, 14);
		frmCadastro.getContentPane().add(label1);
		
		label2 = new JLabel("");
		label2.setBounds(10, 276, 203, 17);
		frmCadastro.getContentPane().add(label2);
		
		jtextfieldnome = new JTextField();
		jtextfieldnome.setBounds(84, 31, 86, 20);
		frmCadastro.getContentPane().add(jtextfieldnome);
		jtextfieldnome.setColumns(10);
		
		jtextfieldemail = new JTextField();
		jtextfieldemail.setBounds(84, 63, 86, 20);
		frmCadastro.getContentPane().add(jtextfieldemail);
		jtextfieldemail.setColumns(10);
		
		lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(10, 65, 70, 15);
		frmCadastro.getContentPane().add(lblEmail);
		
		lblNewLabel = new JLabel("CPF");
		lblNewLabel.setBounds(10, 108, 70, 15);
		frmCadastro.getContentPane().add(lblNewLabel);
		
		jtextfieldcpf = new JTextField();
		jtextfieldcpf.setBounds(85, 106, 86, 19);
		frmCadastro.getContentPane().add(jtextfieldcpf);
		jtextfieldcpf.setColumns(10);
		
		lbltelefone = new JLabel("Telefone");
		lbltelefone.setBounds(10, 135, 70, 15);
		frmCadastro.getContentPane().add(lbltelefone);
		
		jtextfieldtelefone = new JTextField();
		jtextfieldtelefone.setBounds(85, 137, 86, 19);
		frmCadastro.getContentPane().add(jtextfieldtelefone);
		jtextfieldtelefone.setColumns(10);
		
		lblCurso = new JLabel("Curso");
		lblCurso.setBounds(10, 170, 70, 15);
		frmCadastro.getContentPane().add(lblCurso);
		
		jtextfieldcurso = new JTextField();
		jtextfieldcurso.setBounds(85, 168, 86, 19);
		frmCadastro.getContentPane().add(jtextfieldcurso);
		jtextfieldcurso.setColumns(10);
		
		jtextfieldmatricula = new JTextField();
		jtextfieldmatricula.setBounds(84, 197, 90, 19);
		frmCadastro.getContentPane().add(jtextfieldmatricula);
		jtextfieldmatricula.setColumns(10);
		
		lblMatrcula = new JLabel("Matrícula");
		lblMatrcula.setBounds(10, 199, 70, 15);
		frmCadastro.getContentPane().add(lblMatrcula);
		
		button2 = new JButton("Cadastrar Pessoa");
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String nome = jtextfieldnome.getText();
					String email = jtextfieldemail.getText();
					String cpf = jtextfieldcpf.getText();
					String telefone = jtextfieldtelefone.getText();
					String curso = jtextfieldcurso.getText();
					String matricula = jtextfieldmatricula.getText();
					Candidato c = Fachada.cadastrarCandidato(nome,email,cpf,telefone,curso,matricula);
					textFieldtexto.setText("");
					
					if (c == null) {
						label2.setText("problema no cadastro");
					} else {
						label2.setText("cadastro concluido");
					}
//					label2.setText("cadastro concluido");
				}
				catch(Exception e) {
					JOptionPane.showMessageDialog(null,e.getStackTrace());
					
				}
			}
		});
		
//		button1.setBounds(28, 108, 120, 23);
//		frmCadastro.getContentPane().add(button1);
		button2.setBounds(25, 242, 145, 23);
		frmCadastro.getContentPane().add(button2);
		

		
//		scrollPane = new JScrollPane();
//		scrollPane.setBounds(53, 142, 78, 80);
//		frmCadastro.getContentPane().add(scrollPane);
//		
//		model = new DefaultListModel<String>() ;
//		list = new JList<String>(model);
//		scrollPane.setColumnHeaderView(list);
//		
		//mostrar a janela
		frmCadastro.setVisible(true);
	}

}
