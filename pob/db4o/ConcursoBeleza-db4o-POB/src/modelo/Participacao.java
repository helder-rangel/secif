package modelo;

public class Participacao {

	private boolean checkin;
	private boolean certificado;
	private double pontuacao;
	private Atividade atividade;
	private Inscricao inscricao;
	
	public Participacao() {
		super();
	}

	public Participacao(boolean checkin, boolean certificado, double pontuacao) {
		super();
		this.checkin = checkin;
		this.certificado = certificado;
		this.pontuacao = pontuacao;
	}

	public boolean isCheckin() {
		return checkin;
	}

	public boolean isCertificado() {
		return certificado;
	}

	public double getPontuacao() {
		return pontuacao;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public Inscricao getInscricao() {
		return inscricao;
	}

	public void setCheckin(boolean checkin) {
		this.checkin = checkin;
	}

	public void setCertificado(boolean certificado) {
		this.certificado = certificado;
	}

	public void setPontuacao(double pontuacao) {
		this.pontuacao = pontuacao;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}

	@Override
	public String toString() {
		return "Participacao [checkin=" + checkin + ", certificado=" + certificado + ", pontuacao=" + pontuacao
				+ ", atividade=" + atividade + ", inscricao=" + inscricao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result + (certificado ? 1231 : 1237);
		result = prime * result + (checkin ? 1231 : 1237);
		result = prime * result + ((inscricao == null) ? 0 : inscricao.hashCode());
		long temp;
		temp = Double.doubleToLongBits(pontuacao);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Participacao other = (Participacao) obj;
		if (atividade == null) {
			if (other.atividade != null)
				return false;
		} else if (!atividade.equals(other.atividade))
			return false;
		if (certificado != other.certificado)
			return false;
		if (checkin != other.checkin)
			return false;
		if (inscricao == null) {
			if (other.inscricao != null)
				return false;
		} else if (!inscricao.equals(other.inscricao))
			return false;
		if (Double.doubleToLongBits(pontuacao) != Double.doubleToLongBits(other.pontuacao))
			return false;
		return true;
	}

	
	
}
