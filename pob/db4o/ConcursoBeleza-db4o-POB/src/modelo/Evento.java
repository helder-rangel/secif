package modelo;

import java.util.ArrayList;

public class Evento {
	
	private String nome;
	private String ano;
	private ArrayList<Atividade> atividade;
	private ArrayList<Inscricao> inscricao;
	
	public Evento() {
		super();
		this.atividade = new ArrayList<Atividade>();
		this.inscricao = new ArrayList<Inscricao>();
	}

	public Evento(String nome, String ano) {
		super();
		this.nome = nome;
		this.ano = ano;
		this.atividade = new ArrayList<Atividade>();
		this.inscricao = new ArrayList<Inscricao>();
	}

	public String getNome() {
		return nome;
	}

	public String getAno() {
		return ano;
	}

	public ArrayList<Atividade> getAtividade() {
		return atividade;
	}

	public ArrayList<Inscricao> getInscricao() {
		return inscricao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public void setAtividade(ArrayList<Atividade> atividade) {
		this.atividade = atividade;
	}

	public void setInscricao(ArrayList<Inscricao> inscricao) {
		this.inscricao = inscricao;
	}

	@Override
	public String toString() {
		return "Evento [nome=" + nome + ", ano=" + ano + ", atividade=" + atividade + ", inscricao=" + inscricao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result + ((inscricao == null) ? 0 : inscricao.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evento other = (Evento) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (atividade == null) {
			if (other.atividade != null)
				return false;
		} else if (!atividade.equals(other.atividade))
			return false;
		if (inscricao == null) {
			if (other.inscricao != null)
				return false;
		} else if (!inscricao.equals(other.inscricao))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	public void adicionarAtividade(Atividade atividade) throws Exception {
		
		for (Atividade a : this.atividade) {
			if (a.equals(atividade)) {
				throw new Exception("Não e possivel inserir: atividade ja existente");
			}
		}
		
		this.atividade.add(atividade);
		
	}

	public void adicionarInscricao(Inscricao inscricao) throws Exception {
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(inscricao)) {
				throw new Exception("Não e possivel inserir: inscricao ja existente");
			}
		}
		
		this.inscricao.add(inscricao);		
	}
	
	public void alterarAtividade(Atividade atividadeAntigo, Atividade atividadeNovo ) throws Exception {
		
		Atividade atividadeTemporario = null;
		
		for (Atividade a : this.atividade) {
			if (a.equals(atividadeAntigo)) {
				atividadeTemporario = a;
				this.atividade.remove(a);
			}
		}
		
		if (atividadeTemporario == null) {
			throw new Exception("Não e possivel alterar: atividade nao existente");
		}
		
		this.atividade.add(atividadeNovo);		
	}
	
	public void alterarInscricao(Inscricao inscricaoAntigo, Inscricao inscricaoNovo ) throws Exception {
		
		Inscricao inscricaoTemporario = null;
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(inscricaoAntigo)) {
				inscricaoTemporario = i;
				this.inscricao.remove(i);
			}
		}
		
		if (inscricaoTemporario == null) {
			throw new Exception("Não e possivel alterar: inscricao nao existente");
		}
		
		this.inscricao.add(inscricaoNovo);		
	}
	
	public void removerAtividade(Atividade atividade) throws Exception {
		
		for (Atividade a : this.atividade) {
			if (a.equals(atividade)) {
				this.atividade.remove(atividade);
			}
		}
		
		throw new Exception("Não e possivel remover: atividade nao existente");
		
	}

	public void removerInscricao(Inscricao inscricao) throws Exception {
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(inscricao)) {
				this.inscricao.remove(inscricao);
			}
		}
		
		throw new Exception("Não e possivel remover: inscricao nao existente");		
	}
	
	public String listarAtividade() {
		
		String retorno = "Listagem dos Atividades\n";
		
		for (Atividade a : this.atividade) {
			retorno+= a + "\n";
		}
		
		return retorno;
	}
	
	public String listarInscricao() {
		
		String retorno = "Listagem das Inscricoes\n";
		
		for (Inscricao i: this.inscricao) {
			retorno+= i + "\n";
		}
		
		return retorno;
	}

	public Atividade procurarAtividade(Atividade atividade) throws Exception {
		
		for (Atividade a : this.atividade) {
			if (a.equals(atividade)) {
				return a;
			}
		}
		
		throw new Exception("Nao e possivel coletar: atividade nao existente");
	}
	
	public Inscricao procurarInscricao(Inscricao inscricao) throws Exception {
		
		for (Inscricao i : this.inscricao) {
			if (i.equals(atividade)) {
				return i;
			}
		}
		
		throw new Exception("Nao e possivel coletar: inscricao nao existente");
	}
	
}
