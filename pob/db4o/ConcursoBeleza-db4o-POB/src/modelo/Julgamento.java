package modelo;

public class Julgamento {
	
	private double nota;
	private String quesito;
	private Atividade atividade;
	private Juiz juiz; 
	
	public Julgamento() {
		super();
	}

	public Julgamento(double nota, String quesito) {
		super();
		this.nota = nota;
		this.quesito = quesito;
	}

	public double getNota() {
		return nota;
	}

	public String getQuesito() {
		return quesito;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public Juiz getJuiz() {
		return juiz;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}

	public void setQuesito(String quesito) {
		this.quesito = quesito;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public void setJuiz(Juiz juiz) {
		this.juiz = juiz;
	}

	@Override
	public String toString() {
		return "Julgamento [nota=" + nota + ", quesito=" + quesito + ", atividade=" + atividade + ", juiz=" + juiz
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result + ((juiz == null) ? 0 : juiz.hashCode());
		long temp;
		temp = Double.doubleToLongBits(nota);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((quesito == null) ? 0 : quesito.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Julgamento other = (Julgamento) obj;
		if (atividade == null) {
			if (other.atividade != null)
				return false;
		} else if (!atividade.equals(other.atividade))
			return false;
		if (juiz == null) {
			if (other.juiz != null)
				return false;
		} else if (!juiz.equals(other.juiz))
			return false;
		if (Double.doubleToLongBits(nota) != Double.doubleToLongBits(other.nota))
			return false;
		if (quesito == null) {
			if (other.quesito != null)
				return false;
		} else if (!quesito.equals(other.quesito))
			return false;
		return true;
	}

	
	
	
}
