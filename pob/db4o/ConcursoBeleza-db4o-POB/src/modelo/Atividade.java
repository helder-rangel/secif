package modelo;

import java.time.LocalDate;
import java.util.ArrayList;

public class Atividade {

	private String nome;
	private String descricao;
	private double minimoParticipantes;
	private double maximoParticipantes;
	private LocalDate horario;
	private ArrayList<Julgamento> pontuacao;
	private ArrayList<Participacao> participacao;
	private Evento evento;
	
	public Atividade() {
		super();
		this.pontuacao = new ArrayList<Julgamento>();
		this.participacao = new ArrayList<Participacao>();
	}

	public Atividade(String nome, String descricao, double minimoParticipantes, double maximoParticipantes,
			LocalDate horario ) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.minimoParticipantes = minimoParticipantes;
		this.maximoParticipantes = maximoParticipantes;
		this.horario = horario;
		this.pontuacao = new ArrayList<Julgamento>();
		this.participacao = new ArrayList<Participacao>();
	}

	public String getNome() {
		return nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public double getMinimoParticipantes() {
		return minimoParticipantes;
	}

	public double getMaximoParticipantes() {
		return maximoParticipantes;
	}

	public LocalDate getHorario() {
		return horario;
	}

	public ArrayList<Julgamento> getPontuacao() {
		return pontuacao;
	}

	public ArrayList<Participacao> getParticipacao() {
		return participacao;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setMinimoParticipantes(double minimoParticipantes) {
		this.minimoParticipantes = minimoParticipantes;
	}

	public void setMaximoParticipantes(double maximoParticipantes) {
		this.maximoParticipantes = maximoParticipantes;
	}

	public void setHorario(LocalDate horario) {
		this.horario = horario;
	}

	public void setPontuacao(ArrayList<Julgamento> pontuacao) {
		this.pontuacao = pontuacao;
	}

	public void setParticipacao(ArrayList<Participacao> participacao) {
		this.participacao = participacao;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	@Override
	public String toString() {
		return "Atividade [nome=" + nome + ", descricao=" + descricao + ", minimoParticipantes=" + minimoParticipantes
				+ ", maximoParticipantes=" + maximoParticipantes + ", horario=" + horario + ", pontuacao=" + pontuacao
				+ ", participacao=" + participacao + ", evento=" + evento + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((evento == null) ? 0 : evento.hashCode());
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		long temp;
		temp = Double.doubleToLongBits(maximoParticipantes);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minimoParticipantes);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((participacao == null) ? 0 : participacao.hashCode());
		result = prime * result + ((pontuacao == null) ? 0 : pontuacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atividade other = (Atividade) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (evento == null) {
			if (other.evento != null)
				return false;
		} else if (!evento.equals(other.evento))
			return false;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		if (Double.doubleToLongBits(maximoParticipantes) != Double.doubleToLongBits(other.maximoParticipantes))
			return false;
		if (Double.doubleToLongBits(minimoParticipantes) != Double.doubleToLongBits(other.minimoParticipantes))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (participacao == null) {
			if (other.participacao != null)
				return false;
		} else if (!participacao.equals(other.participacao))
			return false;
		if (pontuacao == null) {
			if (other.pontuacao != null)
				return false;
		} else if (!pontuacao.equals(other.pontuacao))
			return false;
		return true;
	}

	public void adicionarJulgamento(Julgamento julgamento) throws Exception {
		
		for (Julgamento j : this.pontuacao) {
			if (j.equals(julgamento)) {
				throw new Exception("Não e possivel inserir: julgamento ja existente");
			}
		}
		
		this.pontuacao.add(julgamento);
		
	}
	
	public void adicionarParticipacao(Participacao participacao) throws Exception {
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacao)) {
				throw new Exception("Não e possivel inserir: participacao ja existente");
			}
		}
		
		this.participacao.add(participacao);		
	}
	
	public void alterarJulgamento(Julgamento julgamentoAntigo, Julgamento julgamentoNovo ) throws Exception {
		
		Julgamento julgamentoTemporario = null;
		
		for (Julgamento j : this.pontuacao) {
			if (j.equals(julgamentoAntigo)) {
				julgamentoTemporario = j;
				this.pontuacao.remove(j);
			}
		}
		
		if (julgamentoTemporario == null) {
			throw new Exception("Não e possivel alterar: julgamento nao existente");
		}
		
		this.pontuacao.add(julgamentoNovo);		
	}
	
	public void alterarParticipacao(Participacao participacaoAntiga, Participacao participacaoNova) throws Exception{
		
		Participacao participacaoTemporaria = null;
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacaoAntiga)) {
				participacaoTemporaria = p;
				this.participacao.remove(p);
			}
		}
		
		if (participacaoTemporaria == null) {
			throw new Exception("Não e possivel alterar: participacao nao existente");
		}
		
		this.participacao.add(participacaoNova);		
	}
	
	public void removerJulgamento(Julgamento julgamento) throws Exception {
		
		for (Julgamento j : this.pontuacao) {
			if (j.equals(julgamento)) {
				this.pontuacao.remove(julgamento);
			}
		}
		
		throw new Exception("Não e possivel remover: julgamento nao existente");
		
	}
	
	public void removerParticipacao(Participacao participacao) throws Exception {
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacao)) {
				this.participacao.remove(participacao);
			}
		}
		
		throw new Exception("Não e possivel remover: participacao nao existente");
	}
	
	public String listarJulgamento() {
		
		String retorno = "Listagem dos Julgamentos";
		
		for (Julgamento j : this.pontuacao) {
			retorno+= "\n" + j;
		}
		
		return retorno;
	}
	
	public String listarParticipacao() {
		
		String retorno = "Listagem das Participacoes\n";
		
		for (Participacao p: this.participacao) {
			retorno+= p + "\n";
		}
		
		return retorno;
	}	
	
	public Julgamento procurarJulgamento(Julgamento julgamento) throws Exception {
		
		for (Julgamento j : this.pontuacao) {
			if (j.equals(julgamento)) {
				return j;
			}
		}
		
		throw new Exception("Nao e possivel coletar: julgamento nao existente");
	}
	
	public Participacao procurarParticipacao(Participacao participacao) throws Exception {
		
		for (Participacao p : this.participacao) {
			if (p.equals(participacao)) {
				return p;
			}
		}
		
		throw new Exception("Nao e possivel coletar: participacao nao existente");
	}
}
