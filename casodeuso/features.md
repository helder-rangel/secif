# Características iniciais do aplicativo  

| Aluno | Matrícula |
| ----------- | ----------- |
| Aleff  Lima   | 20172370013 |  
| Helder Rangel | 20161370009 |  
| Lucas Nóbrega | 20151370367|


A Semana de Esporte e Cultura do IFPB (Secif), que é o maior evento de atividades culturais e esportivas do IFPB Campus João  Pessoa, terá um
aplicativo, na edição desse ano.  

Aplicativo **Secif** deve gerenciar processo de inscrição on-line no evento. Ele vai usar framework Laravel e banco de dados MySQL, como também o framework vue. 

Ao menos dois funcionários do instituto serão os gestores do aplicativo. Haverá algumas dezenas de usuários julgadores e credenciadores. Evento pode ter centenas de competidores, entre estudantes do ensino médio e superior. Funcionários públicos e terceirizados da instituição poderão participar da atividade Talento. 

As funcionalidades iniciais foram obtidas pelas regras da competição, cujo documento foi entregue em Portable Document Format (PDF). A partir dele começamos a produzir as histórias dos usuários, então percebemos que o aplicativo completo teria quase uma centena de funcionalidades. Em uma segunda oportunidade, o gestor do evento aceitou a proposta de entrega das seguintes funcionalidades, mais focadas na atividade **Beleza**:

- Inscrever participantes;  
- Credenciar inscritos;  
- Registrar pontuações Garota e Garoto Secif;  
- Classificar Garota e Garoto Secif;  
- Emitir certificados;  
- Enviar mensagens por e-mail.  

Sem tratar da classificação dos competidores, critérios de desempate, nem mesmo de detalhes como cartões amarelos, expulsões, confrontos diretos etc,as seguintes atividades terão registros de resultados e participantes, com informações relevantes para a história do evento:

- Atletismo; 
- Badminton; 
- Basquete;
- Foguetes;
- Futebol; 
- Futsal; 
- Gincana;
- Interprete;
- Natação; 
- Robótica;  
- Talento;  
- Voleibol;  
- Xadrez. 

Requisitos não funcionais:  

- Controlar mudanças dos bancos via migração;  
- Mapear relacionamentos entre entidade do banco usando Eloquent;  
- Uso de validações do Laravel;  
- Gerenciar controle de acesso por autenticação;  
- Utilizar no mínimo uma extensão inédita do Laravel;  
- Fazer deploy da aplicação por meio de Docker;  
- Validar e verificar por teste;  
- Adequar a dispositivos móveis;  
- Tolerar credenciamento e inscrições simultâneas;  
- Menores de idade devem entregar autorização de responsável;  
- Desacoplar frontend do backend;  

