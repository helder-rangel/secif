# Secif  
This repository contains the code for development of Secif and artfacts.

[SecifApp](http://www.joaopessoa.ifpb.edu.br/secif) is a aplication which enables inscriptions of members in event, confirmation of presence of member, view calendar of events and emit certificate of participation how competitor, judge or voluntar.  

SecifApp is enable for web. The frontend and backend is Laravel, uses MySQL Database System.

#  Functional requirements  
**Competições culturais**  

[beleza - ](cultura/beleza.md)  

Item	Requisitos não funcionais  
1	[Estrutura funcionamento da aplicação por meio do MVC em Laravel](https://blog.caelum.com.br/seja-um-artesao-da-web-com-laravel-o-framework-mvc-do-php/)  
2	[Controlar mudanças dos bancos via migração](https://imasters.com.br/desenvolvimento/criando-migrations-e-relacionando-tabelas-com-laravel)  
3	[Mapear relacionamentos entre entidade do seu banco usando Eloquent](https://imasters.com.br/banco-de-dados/aprenda-9-truques-para-o-eloquent-laravel)  
4	[Uso de validações do Laravel](https://www.dialhost.com.br/blog/laravel-5-7-criando-form-validations/)  
5	[Gerenciar controle de acesso por autenticação](https://magazine.softerize.com.br/tutoriais/php/laravel/usuarios-e-controle-de-acesso-laravel)  
6	[Utilizar no mínimo uma extensão inédita do Laravel](https://appdividend.com/2017/05/08/generate-pdf-blade-laravel-5-4/)  
7	[Fazer deploy da aplicação por meio de Docker](https://medium.com/trainingcenter/construindo-o-seu-ambiente-de-desenvolvimento-laravel-5-5-com-docker-a5d4bf313aef)  
8	Validação e verificação por teste  

# Referências  
[Resumo Laravel](https://laravel.gen.tr/cheatsheet/)  
[PHP do Jeito Certo](http://br.phptherightway.com)  
[Envio de Logs PHP para arquivos, sockets, e-mail, base de dados e web services](https://github.com/Seldaek/monolog)  
[PHPUnit](https://phpunit.de)  
[Packagist](https://packagist.org/explore/)  
[Documentação Oficial](https://www.php.net/manual/pt_BR/)  
[Padronização em PHP](https://www.php-fig.org/psr/)  
[Protocolo HTTP ](https://nandovieira.com.br/entendendo-um-pouco-mais-sobre-o-protocolo-http)  



# Para implementar  

1. Aleff e Lucas: [API Laravel, consumo de crud com vue](https://appdividend.com/2018/11/17/vue-laravel-crud-example-tutorial-from-scratch/)  
2. Helder e Max: [Behat, estudo da implementação](https://github.com/Behat/Behat)  
3. Aleff, Helder, Lucas, Max: [Instalação de Laravel, de acordo com documentação oficial:todos](https://laravel.com/docs/5.8)  
4. Helder e Max: [Geração de PDF em Laravel](https://appdividend.com/2017/05/08/generate-pdf-blade-laravel-5-4/)

# Para investigar  

+ Como restringir inscricoes para tipos de alunos como integrado, técnico, superior?  
*Fazendo validação, com switch, a partir de dados adquiridos do Suap*.
+ Como fazer lista de espera? Deve-se implantar cancelamento de participação em evento?  
+ Como fazer a classificação de competidores?  
+ Como implantar JWT para autenticar usuários via suap?  
*[jwt](https://blog.pusher.com/laravel-jwt/)*  
+ Quais dados o sistema receberá do Suap?
Nome completo, curso, data de nascimento, sexo, e-mail,matricula, cargo.  
+ Quais plugins usar no Vscode?  
*PHP Getters & Setters, PHP IntelliSense, PHP Debug e Code Runner*.  
+ Seria vantajoso usar versão 8 do MySQL? 
+ Como modelar MVC? O que falta? 
[MVC](https://medium.com/@sagarmaheshwary31/laravel-5-8-from-scratch-intro-setup-mvc-basics-and-views-74d46f93fe0c)  
+ Como gerar certificados em pdf com Laravel?  
+ Quais serão os cruds do gestor Secif? Algum recurso eventual?
+ Que tal usar *carbon*, uma classe que herda comportamentos do PHP DateTime?  
*[carbon][(https://carbon.nesbot.com/docs/)*  
+ Qual estratégia usar para entregar projeto funcionando no final de agosto?  
+ Como implementar envio de e-mails?  
*[emails](https://laravel.com/docs/5.2/mail)*  
+ Como implementar o DAO com Laravel?  

+ Como bloquear cadastro em eventos simultaneos, que ocorrem em mesmo horário?  
*Classe responsável por manter o estado de horarios daquele participante, onde coleta uma referência do banco de dados geral.*


# Diagrama de classes
<img src="imagens/DiagramaDeClasses.png">  
  
 # Diagramas finais
 [diagramas - ](docs/Secif.pdf)